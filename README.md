360 External AngularJS App
===============================
See alpha.sh or prod.sh scripts to deploy to EC2 instances.

## Branch mapping to Environments

| App                         | Environment | Branch     |
|-----------------------------|-------------|------------|
| 360 External AngularJS App  | prod        | master     |


### merge branches to 360-external
#### Step 1: Checkout Master
```
git checkout master
```
#### Step 2: Merge changes
```
git merge {rapidLaunchFeature}
```

#### Deploy to Alpha
Run `./alpha.sh`

#### Deploy to Prod
Run `./prod.sh`