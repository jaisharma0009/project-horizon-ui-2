var template = require('./template.html');
var OverviewController = require('./controller');

let OverviewComponent = {
  restrict: 'E',
  template,
  controller: OverviewController,
  controllerAs: 'overview'
};

module.exports = OverviewComponent;