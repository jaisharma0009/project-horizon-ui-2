var template = require('./template.html');
var MagellanController = require('./controller');

let MagellanComponent = {
  restrict: 'E',
  template,
  bindings: { accountId: '=' },
  controller: MagellanController,
  controllerAs: 'magellan'
};

module.exports = MagellanComponent;