'use strict';

var angular = require('angular'),
  angularMoment = require('angular-moment'),
  angularPromiseTracker = require('angular-promise-tracker'),
  angularUiRouter = require('@uirouter/angularjs').default,
  angularBootstrap = require('angular-ui-bootstrap');

module.exports = angular.module('app.customers.detail', [
    angularMoment,
    angularPromiseTracker,
    angularUiRouter,
    angularBootstrap,
    require('tsbusy').name,
    require('jqsparkline').name,
    require('./magellan').name,
    require('./expansion-rating').name,
    require('./technical-competency').name,
    require('./overview').name    
  ])
  .config(function($stateProvider, $urlRouterProvider) {
    // make sure the default tab is loaded
    $urlRouterProvider.when('/customers/{accountId}',
      '/customers/{accountId}/overview');

    $stateProvider
      .state('customers.detail', {
        url: '/{accountId}',
        component: 'detail',
        resolve: {
          accountId: function($stateParams) {
            return $stateParams.accountId;
          }
        }
      });
  })
  .component('detail', require('./detail.component'));
