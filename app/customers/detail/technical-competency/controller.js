'use strict';

var moment = require('moment');
var $ = require('jquery');
var d3 = require('d3');
var _ = require('lodash');
var solve = require('logician');
var isNumber = require('is-number');


/**
 * @ngInject
 */
module.exports = class TechnicalCompetencyController { 
    constructor($rootScope, $state, $stateParams, $scope, $q, $location, tsAuthService, $interval,
    promiseTracker, contextService, csRestangular, $window, $timeout, $uibModal, $sce) {
    'ngInject';
    
    var accountId = $stateParams.accountId;
    $scope.accountId = accountId;

    $scope.myNumberFilter = function (searchTerm, cellValue) {
	    // Custom logic that returns true if `row`
	    // passes the filter and false if it should
	    // be filtered out
	    if(searchTerm == null) return true;

	    if(cellValue == null) return false;

	    if(isNumber(searchTerm)){
	      return searchTerm == cellValue
	    }

	    cellValue = parseFloat(cellValue);    

	    return solve(cellValue+' '+searchTerm);
	  }

    
    $scope.gridBundleLintsOptions = {
        gridName: 'TechCompetencyGrid',
        data: [],
        paginationPageSize: 25,
        paginationPageSizes: [25, 50, 75],
        enablePaginationControls: true,
        enableFiltering: true,
        enableColumnMenus: true,
        exporterCsvFilename: 'myFile.csv',
        enableGridMenu: true,
        rowHeight: 30,
        columnDefs: [
            {
                displayName: 'Created',
                field: 'createddate',
                type: 'date',
                cellFilter: 'date:"yyyy-MM-dd"',
                width: '10%',
                minWidth: 100
            }, {
                displayName: 'Org',
                field: 'org_name',
                cellTooltip: true,
                width: '15%',
                minWidth: 100
            }, {
                displayName: 'API',
                field: 'api_name',
                cellTooltip: true,
                width: '15%',
                minWidth: 50
            }, {
                displayName: 'Rev',
                field: 'revision',
                headerTooltip: true,
                width: '5%',
                minWidth: 60
            // }, {
            //     displayName: 'Score',
            //     field: 'score',
            //     cellFilter: 'number: 2',
            //     type: 'number',
            //     width: '15%',
            //     cellTooltip: true,
            //     // cellTemplate: '<a ng-if="row.entity.normalized_score > 0" role="button" class="ui-grid-cell-contents" ng-click="grid.appScope.showLintResults(row.entity.id);">{{ row.entity.normalized_rating }} </a>'
            // }, {
            //     displayName: 'Normalized',
            //     field: 'normalized_score',
            //     cellFilter: 'number: 2',
            //     type: 'number',
            //     width: '15%',
            //     cellTooltip: true,
            //     // cellTemplate: '<a ng-if="row.entity.normalized_score > 0" role="button" class="ui-grid-cell-contents" ng-click="grid.appScope.showLintResults(row.entity.id);">{{ row.entity.normalized_rating }} </a>'
            }, {
                displayName: 'Rating',
                field: 'bundle_score',
                width: '20%',
                type: 'number',
                filter: {'condition': $scope.myNumberFilter},
                cellTooltip: true,
                minWidth: 50,
                cellTemplate: '<ts-html-modal class="ui-grid-cell-contents htmlModal" ng-if="row.entity.bundle_id" promise="grid.appScope.getBundleLint(row.entity.bundle_id)" json-path="entity_json.obj.htmlReport">View Lint</ts-html-modal>'+
                              '<a role="button" class="ui-grid-cell-contents" ng-click="grid.appScope.doLint($event, row, row.entity.org_name, row.entity.api_name, row.entity.revision);">Run Linter</a>',
                cardClass: 'lint-links'
            }, {
                displayName: 'Treatments',
                minWidth: 50,
                field: 'bundle_treatments',
                cellTooltip: true,
                // cellTemplate: '<span ng-if="row.entity.bundle_score > 0" class="ui-grid-cell-contents">{{ row.entity.bundle_label }} {{ row.entity.bundle_score }}</span>'
            }
        ],
        onRegisterApi: gridOpts => {
            $scope.gridid = gridOpts.grid.id;
            $interval(gridOpts.core.handleWindowResize, 500);
        }
    };

    $scope.cardsView = false;
    $scope.toggleView = function() {
        $scope.cardsView = !$scope.cardsView;
        $rootScope.$broadcast('TOGGLE_VIEW', $scope.gridid);
    }

    $scope.bundleLintTracker = new promiseTracker();

    $scope.getBundleLint = function(id){
        // debugger;
        return contextService.getBundleLint(id);
    }

    $scope.doLint = function(evt, row, org_name, api_name, revision){        

        var target = evt.currentTarget;
        target.text = 'Linting...';
        var thisRow = row;
        var thisIdBase = ['org', org_name, 'api', api_name, 'revision', revision, 'bundlelint'].join('||') + '||';

        return csRestangular
            .one('edge')
            .one('orgs', org_name)
            .one('apis', api_name)
            .one('revisions', revision)
            .all('lint')
            .post({})
            .then(function(results){
                target.text = 'Success! Run Again?';
                thisRow.entity.bundle_id = thisIdBase + results.results.identity;
                // var view_link = false;
                // if(target.previousSibling){
                //     if(target.previousSibling.previousSibling){
                //         view_link = target.previousSibling.previousSibling;
                //         debugger;
                //     }
                // }
                // var options = {
                //     src: results.htmlReport,
                //     height: $window.innerHeight - 70
                //   };
            
                //   var modalInstance = $uibModal.open({
                //     animation: true,
                //     ariaLabelledBy: 'modal-title',
                //     ariaDescribedBy: 'modal-body',
                //     template: `<iframe style="width: 100%; height:{{height}}px" id="modalIframe" srcdoc="{{src}}"></iframe></div>`,
                //     size: 'lg',
                //     resolve: {
                //       options: function () {
                //         return options;
                //       }
                //     },
                //     controller: function ($scope, $uibModalInstance, options, $sce) {
                //     // debugger;
                //       $scope.src = $sce.trustAsHtml(options.src);
                //       $scope.height = options.height
            
                //       $scope.close = function () {
                //         $uibModalInstance.dismiss();
                //       }
                //     }
                // });            

            })
            .catch(function(err){
                console.error(err);
                target.text = 'Error! Run Again?';
           });

    }

    var bundlLints = contextService.getLatestBundleLints(accountId)   
    var apiProxies = contextService.getApiProxyList(accountId);

    $scope.bundleLintTracker.addPromise([bundlLints, apiProxies]);

    $q.all([bundlLints, apiProxies])
        .then(results => {

            var lints = results[0];
            var apiProxies = results[1];

            lints = _.keyBy(lints, lint => {
                return [lint.org_name, lint.api_name, lint.revision].join('.');
            });

            // console.log(lints);

            _.each(apiProxies, proxy=>{
                var id = [proxy.org_name, proxy.api_name, proxy.revision].join('.');
                var lint = lints[id];
                if(lint !== undefined){
                    // console.log(lint);
                    proxy['bundle_score'] = lint['score'];
                    proxy['bundle_normalized_score'] = lint['normalized_score'];
                    
                    if(typeof lint.treatments == 'string') {
                        lint.treatments = JSON.parse(lint.treatments);
                    }

                    proxy['bundle_treatments'] = lint.treatments.join(', ');
                    proxy['bundle_id'] = lint.id;
                    var score = lint.normalized_score;
                    if(score > 4) score = 4;
                    if(score < 1) score = 0;
                    proxy['bundle_normalized_rating'] = ['Lagging','Lagging','Progressing','Competitive','Leading'][score];
                }else{
                    proxy['bundle_id'] = null
                    proxy['bundle_score'] = null;
                    proxy['bundle_normalized_score'] = null;
                    proxy['bundle_normalized_rating'] = 'No Recent Analysis'
                }
            });

            $scope.gridBundleLintsOptions.data = apiProxies;
            
        });
    
    $timeout(function(){
        var tab = $('#overviewTab');
        tab.removeClass('active');
    }, 250);

    }
};