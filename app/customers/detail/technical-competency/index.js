'use strict';

var angular = require('angular'),
  angularMoment = require('angular-moment'),
  angularPromiseTracker = require('angular-promise-tracker'),
  angularUiRouter = require('@uirouter/angularjs').default,
  uiGrid = require('angular-ui-grid').default,
  uib = require('angular-ui-bootstrap');

  var section = 'technical-competency';

  var identifier = 'customers.detail.'+section;  
  var ctrlr_id = identifier.split('.').join('')+'ctrl';
  

module.exports = angular.module('app'+identifier, [
    angularMoment,
    angularPromiseTracker,
    angularUiRouter,
    'ui.bootstrap',
    'ui.grid',
    'ui.grid.exporter',
    'ui.grid.pagination',
    require('tsbusy').name,
    require('tsuigrid').name,
    require('tshtmlmodal').name,
    require('jqsparkline').name,
    require('radarchart').name
])
  .config(function($stateProvider) {
    $stateProvider
      .state(identifier, {
        url: '/'+section,
        component: 'technicalCompetency'
      });
  })
  .component('technicalCompetency', require('./technical-competency.component'));

