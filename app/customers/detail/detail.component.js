var template = require('./template.html');
var DetailController = require('./controller');

let DetailComponent = {
  restrict: 'E',
  bindings: { accountId: '=' },
  template,
  controller: DetailController,
  controllerAs: 'detail'
};

module.exports = DetailComponent;