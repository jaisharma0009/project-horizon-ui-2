'use strict';

var angular = require('angular');
var $ = require('jquery');
var uuidGen = require('../utils/uuidGen');

/**
 * Directive for custom selects
 */
module.exports = angular.module('app.components.tscollapsable', [])
  .directive('tsCollapsable', function ($rootScope) {
    return {
      restrict: 'A',
      scope: {
        tscOn: '@',
        tscOff: '@',
        tscCols: '@',
        tsExport: '@'
      },
      link: function (scope, element, attrs, ctrl) {
        let onTxt = scope.tscOn || '';
        let offTxt = scope.tscOff || '';
        let manual = false;
        let min_width = 1200;
        let tscCols = scope.tscCols || false;
        let tscExport = scope.tscExport || false;

        let uuid = uuidGen();

        let cols = `<span id="tsc-columns" style="display:none">
                      Columns<i class="fa fa-caret-up" aria-hidden="true"></i>
                    </span>`;
        
        let filters = `<span id="tsc-filters" style="display:none">
                      Filters<i class="fa fa-caret-up" aria-hidden="true"></i>
                      </span>`;
              
        let exp = `<span id="tsc-export" style="display:none">
                      Export<i class="fa fa-caret-up" aria-hidden="true"></i>
                    </span>`;
        
        if(!tscCols) cols = '';
        if(!tscExport) exp = '';

        let cBtn = `<div class="collapsePnl" id="${uuid}">
                      ${cols}${filters}${exp}
                      <div class="collapseBtn">
                        <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                      </div>
                    </div>`;

        $(element).prepend(cBtn);

        $(document).ready(function () {
          screenSizeCheck();
          $(window).resize(screenSizeCheck);
        });

        function screenSizeCheck() {
          let width = $(document).width();
          if (width <= min_width) {
            hideContents();
            $('.collapseBtn').hide();
          } else {
            if(!manual) {
              showContents();
            }
            $('.collapseBtn').show();
          }
        }

        $(`#${uuid}`).find('.collapseBtn').click(() => {
          manual = !manual;
          $(element).height() === 0 ? showContents() : hideContents();
        });

        $('#tsc-export').click(() => {
          $(element).find('#export-compact-menu').toggle();
          $(element).find('#tsc-export i').toggleClass('fa-caret-up');
          $(element).find('#tsc-export i').toggleClass('fa-caret-down');
        });

        $('#tsc-filters').click(() => {
          $(element).find('.filters-container').toggle();
          $(element).find('#tsc-filters i').toggleClass('fa-caret-up');
          $(element).find('#tsc-filters i').toggleClass('fa-caret-down');
        });

        $('#tsc-columns').click(() => {
          let m = $(element).find('.grid-columns-dropdown');
          let display = m.css('display');
          display == 'none' ? m.removeClass('ng-hide') : m.addClass('ng-hide');

          $(element).find('#tsc-columns i').toggleClass('fa-caret-up');
          $(element).find('#tsc-columns i').toggleClass('fa-caret-down');
        });

        function hideContents() {
          $(element).children().not('.nonCollapse').not('.collapsePnl').not('#columns-menu').hide();
          $(element).find('.cards-grid-toggle').addClass('collapsed');
          $('#tsc-filters').show();
          $('#tsc-columns').show();
          $('#tsc-export').show();
          $(element).find('.filters-container').addClass('compact-filters');
          $(element).find('.collapsePnl').addClass('absoluted');
          $(element).find('.collapsePnl').attr('title', onTxt);
          $(`#${uuid}`).find('.collapseBtn i').removeClass('fa-angle-double-down');
          $(`#${uuid}`).find('.collapseBtn i').addClass('fa-angle-double-up');
          $('.ts-collapse').addClass('small-header');
          $('#columns-menu').addClass('minimized');
          $rootScope.$broadcast('TSCOLLAPSE_HIDE');
        }

        function showContents() {
          $('.collapseBtn').show();
          $(element).children().not('#export-compact-menu').show();
          $(element).find('.cards-grid-toggle').removeClass('collapsed');
          $('#tsc-filters').hide();
          $('#tsc-columns').hide();
          $('#tsc-export').hide();
          $(element).find('.filters-container').removeClass('compact-filters');
          $(element).find('.collapsePnl').removeClass('absoluted');
          $(element).find('.collapsePnl').attr('title', offTxt);
          $(`#${uuid}`).find('.collapseBtn i').removeClass('fa-angle-double-up');
          $(`#${uuid}`).find('.collapseBtn i').addClass('fa-angle-double-down');
          $('.ts-collapse').removeClass('small-header');
          $('#columns-menu').removeClass('minimized');
          $rootScope.$broadcast('TSCOLLAPSE_SHOW');
        }
      }
    };
  });
