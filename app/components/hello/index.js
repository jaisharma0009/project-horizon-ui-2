'use strict';

var angular = require('angular');

module.exports = angular.module('app.components.hello', [])
  .controller('HelloCtrl', require('./controller'));
