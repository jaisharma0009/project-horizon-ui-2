'use strict';

var angular = require('angular');

/**
 * Directive for displaying a product name
 */
module.exports = angular.module('app.components.tsjiralink', [])
  .directive('tsJiralink', function() {
    return {
      restrict: 'E',
      scope: {
        jira_issue_no: '='
      },
      template: require('./template.html'),
      controller: 'tsJiraLinkCtrl'
    };
  })
  .controller('tsJiraLinkCtrl', require('./controller'));
