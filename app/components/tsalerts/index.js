'use strict';

let angular = require('angular'),
    angularBootstrap = require('angular-ui-bootstrap');
let register = require('../directive.register.js');

import TsAlertsDirective from './ts-alerts.directive';
/**
 * Directive and service for displaying alerts
 */
module.exports = angular.module('app.components.tsalerts', [
  angularBootstrap
])
  .factory('tsAlertsService', require('./service'))
  .directive('tsAlerts', register(TsAlertsDirective));
