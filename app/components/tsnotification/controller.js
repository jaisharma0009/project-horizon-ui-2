'use strict';

/**
 * @ngInject
 */
module.exports = function($scope) {
  $scope.isCollapsed = true;
  $scope.btnToggleText = 'Show';
  $scope.toggleBtnText = function () {
    if ($scope.btnToggleText == 'Show') {
      $scope.btnToggleText = 'Hide';
    } else {
      $scope.btnToggleText = 'Show';
    }
  };
};
