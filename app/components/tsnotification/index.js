'use strict';

var angular = require('angular');

/**
 * Directive and service for displaying a warning or info notification
 */
module.exports = angular.module('app.components.tsnotification', [])
  .directive('tsNotification', function() {
    function link(scope, element, attrs) {
      scope.type = {};
      switch (attrs.type) {
        case 'info':
          scope.type.class = 'material-icons info';
          scope.type.text = 'error_outline';
          break;
        case 'warning':
          scope.type.class = 'material-icons warning';
          scope.type.text = 'warning';
          break;
      }
    }
    return {
      restrict: 'E',
      scope: {
        buttontext: '@',
        title: '@'
      },
      transclude: true,
      template: require('./template.html'),
      controller: 'tsNotificationCtrl',
      link: link
    };
  })
  .controller('tsNotificationCtrl', require('./controller'));
