'use strict';

var $ = require('jquery');
var template = require('./template.html');

module.exports = angular.module('app.components.uigridcolsdropdown', [])
  .directive('uiGridColsDropdown', function ($rootScope, $timeout, $compile) {
    return {
      restrict: 'E',
      scope: {
        cols: '=colDef',
        api: '=gridApiObj'
      },
      template: template,
      link: function(scope, element, attrs) {
        scope.visibleGridDropdown = {visible: false };
        console.log(scope.cols)

        scope.toggleFilter = function(index) {
          
        }
      }
    }
  });