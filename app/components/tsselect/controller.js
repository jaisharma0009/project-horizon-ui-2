'use strict';
var $ = require('jquery');

module.exports = function ($scope, $rootScope, $timeout, $element) {
  'ngInject';

  $scope.id = semiGuid();
  $scope.showDrop = false;

  $scope.offset = $scope.offset*1 || 0;
  $scope.offset += $element[0].offsetHeight;
  $scope.offsetStyle = { top: `${$scope.offset}px` }

  $element.on('click', (e) => {
    e.stopPropagation();
  });

  $(document).on('click', () => {
    $rootScope.$broadcast('TSS_SELECT_OPEN');
    $timeout(() => {
      $rootScope.$broadcast('TSS_SELECT_CLOSE');
    }, 20);
  });

  $scope.clickedItem = item => {
    if ($scope.selectValue) {
      $scope.model = item[$scope.selectValue];
    } else {
      $scope.model = item;
    }

    $rootScope.$broadcast('TSS_CLICK');
    $scope.toggleDrop();
  }

  $scope.parseItem = item => {
    if(item) {
      return $scope.display ? item[$scope.display] : item;
    } else {
      return "";
    }
  }

  $scope.toggleDrop = state => {
    if(state == null) {
      $scope.showDrop = !$scope.showDrop;
    } else {
      $scope.showDrop = state;
    }

    if ($scope.showDrop) {
      $rootScope.$broadcast('TSS_SELECT_OPEN', $scope.id);
    } else {
      $rootScope.$broadcast('TSS_SELECT_CLOSE', $scope.id);
    }
  }

  $scope.$on('TSS_SELECT_CLOSE', (evnt, id) => {
    $(`#tss-${$scope.id}`).removeClass('unfocused');
  })

  $scope.$on('TSS_SELECT_OPEN', (evnt, id) => {
    if(id !== $scope.id) {
      $scope.toggleDrop(false);

      $timeout(() => {
        $(`#tss-${$scope.id}`).addClass('unfocused');
      }, 1);
    }
  });

  function semiGuid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4();
  }
};
