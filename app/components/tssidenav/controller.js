'use strict';

/**
 * @ngInject
 */
module.exports = function($scope, $http, tsAuthService) {
	$scope.tsAuth = tsAuthService;
	
	var navItems = {
		"Overview": "customers.detail.overview",
		"Usage": "customers.detail.usage",
		"Health": "customers.detail.health",
		"Support Cases": "customers.detail.support",
		"Purchase History": "customers.detail.purchases",
		"Contacts": "customers.detail.contacts",
		"Rapid Launch": "customers.detail.launch"
	};

	// var navItems = {
	// 	"Overview": "customers.detail.overview",
	// 	"Usage": "customers.detail.usage",
	// 	"Health": "customers.detail.health",
	// 	"Support Cases": "customers.detail.support",
	// 	"Purchase History": "customers.detail.purchases",
	// 	"Contacts": "customers.detail.contacts",
	// 	"360 Users": "customers.detail.users"
	// };

	$scope.getNavText = function () {
		return Object.keys(navItems);
	}

	$scope.getNavLink = function (key) {
		return navItems[key];
	}

};
