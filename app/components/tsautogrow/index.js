'use strict';

var angular = require('angular');
var uib = require('angular-ui-bootstrap');
var $ = require('jquery');
/**
 * Directive for displaying a product name
 */
module.exports = angular.module('app.components.tsautogrow', [])
  .directive('tsAutogrow', function ($timeout) {
    return {
      restrict: 'A',
      scope: false,
      link: function (scope, element, attrs, ctrl) {
        function resize() {
            var value = element.val();
            var width = Math.floor((value.length + 1) * 8.6);
            $(element).width((width - 5));
        }

        $(document).ready(function() {
          resize();

          $(element).on('keydown', resize);
        });
      }
    };
  });
