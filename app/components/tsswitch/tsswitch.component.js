let template = require('./tsswitch.template.html');
let TsswitchController = require('./tsswitch.controller');

let TsSwitchComponent = {
  restrict: 'E',
  template,
  controller: TsswitchController,
  controllerAs: '$ctrl',
  bindings: {
    id: '@',
    initialState: '=',
    btnClass: '@',
    label: '@',
    onText: '@',
    offText: '@',
    ngOn: "<",
    ngOff: "<"
  }
};

module.exports = TsSwitchComponent;
