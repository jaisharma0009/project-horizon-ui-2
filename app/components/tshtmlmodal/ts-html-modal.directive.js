'use strict';

export default class TsHtmlModalDirective {
  constructor() {}

    restrict = 'E';
    scope = {
    size: '@',
    jsonPath: '@',
    promise: '&',
    onClose: '&',
    onDismiss: '&'
  }
  transclude = true;
  template = require('./template.html');
  controller = require('./controller');
  
}
