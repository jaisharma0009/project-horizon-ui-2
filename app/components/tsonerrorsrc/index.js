'use strict';

var angular = require('angular');
/**
 * Directive for displaying a checkmark indicator
 */
module.exports = angular.module('app.components.tsonerrorsrc', [])
  .directive('tsOnErrorSrc', function() {
    return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            console.log('error src!');
            if (attrs.src != attrs.tsOnErrorSrc) {
              attrs.$set('src', attrs.tsOnErrorSrc);
            }
          });
        }
    }
});
