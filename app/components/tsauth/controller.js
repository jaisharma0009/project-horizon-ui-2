'use strict';

/**
 * @ngInject
 */
module.exports = function($scope, tsAuthService) {
  $scope.tsAuth = tsAuthService;

  if(ENV == 'local') {
    $scope.tsAuth.user.picture = 'https://developers.google.com/experts/img/user/user-default.png';
    $scope.tsAuth.user.email = 'test.email@google.com';
  }
};
