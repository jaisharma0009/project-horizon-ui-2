/**
 * Generates a UUID
**/
let s4 = () => Math.floor((1 + Math.random()) * 0x10000)
                          .toString(16).substring(1);

let uuidGen = () => s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();

module.exports = uuidGen;
