'use strict';

var angular = require('angular');

/**
 * Directive for custom selects
 */
module.exports = angular.module('app.components.tspercentage', [])
  .directive('tsPercentage', function() {
    return {
      restrict: 'E',
      scope: {
        total: '@',
        value: '@',
        type: '@',
        percentage: '@'
      },
      template: require('./template.html'),
      link: function (scope, element, attrs, ctrl) {
        scope.total = scope.total * 1;

        if(!scope.percentage) {
          let percentage = scope.value * 100 / scope.total;
          percentage =  Math.round(percentage/5)*5;
          scope.percentage = percentage;
        }

        scope.array = [];
        for(let i=1; i <= scope.total; i++) {
          scope.array.push(i);
        }
      }
    };
  });
