'use strict';

var angular = require('angular');

/**
 * Directive for displaying a product name
 */
module.exports = angular.module('app.components.tsplatform', [])
  .directive('tsPlatform', function() {
    return {
      restrict: 'E',
      scope: {
        platform: '='
      },
      template: require('./template.html'),
      controller: 'tsPlatformCtrl'
    };
  })
  .controller('tsPlatformCtrl', require('./controller'));
