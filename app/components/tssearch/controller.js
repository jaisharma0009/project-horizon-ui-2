'use strict';

/**
 * @ngInject
 */
module.exports = function($scope, $http) {

  $scope.showSearch;
  $scope.query;
  $scope.searchResults;  
  $scope.pages = [];
  $scope.currentPage;
  $scope.searchWindowClass;
  $scope.blurPage;

  // Check if user is on a mobile device
  var mobileUser = function() {
    var check = false;
    (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);

    if (screen.height < 400 && screen.width < 650) {
      check = true;
    }
    return check;
  }

  $scope.executeSearch = function (page, filter) {
    var gss_url;
    var excludedSites = '-site:community.apigee.com/topics -site:community.apigee.com/spaces -site:community.apigee.com/academy -site:community.apigee.com/themes'
    var query = $scope.query;

    $scope.blurPage = true;
    $scope.searchWindowClass = 'search-window blurred';

    if (filter) {
      query += ' more:' + filter;
    }

    if (page) {
    	var startIndex;
    	if (page == 'next') {
    		startIndex = (($scope.currentPage) * 10) + 1;
    	} else if (page == 'prev') {
    		startIndex = (($scope.currentPage - 2) * 10) + 1;
    	} else {
	      startIndex = ((page - 1) * 10) + 1;	      
	    }
	    
	    if(startIndex < 1) {
	    	startIndex = 1;	
	    } else if (startIndex > 91) {
	    	startIndex = 91;
	    }

	    $scope.currentPage = page;
	    query += '&startIndex=' + startIndex;
    } else {
    	$scope.currentPage = 1;
    }

    gss_url = 'https://apigeedocs-prod.apigee.net/community_search/search/?q=' + query + ' ' + excludedSites;

    $http.get(gss_url).
    	success(function (response) {

      for (var i in response.results) {
	      //remove '| Apigee Documentation' from the title
	      var title = response.results[i].title;
	      var link = response.results[i].link;	      
	      var site = parseSiteFromURL(link);
	      var snippet = response.results[i].snippet;
	      
	      response.results[i].title = appendSiteToTitle(title, site);
	      response.results[i].linkText = truncateUrl(link);	      
	    }

	    $scope.searchResults = response.results;
	    
	    appendPagination(response);
      $scope.searchWindowClass = 'search-window';
	    $scope.showSearch = true;

    });
  }

  var appendPagination = function (response) {

    var paginationData;
    var numPages;
    var pageArray;

    $scope.pages = [];

    if(response.nextPage) {
      paginationData = response.nextPage;
    } else if (response.previousPage) {
      paginationData = response.previousPage;
    }

    if (paginationData.totalResults < 100) {
      numPages = Math.floor(paginationData.totalResults/10);
    } else {
      numPages = 10;
    }

    for (var i = 0; i < numPages; i++) {
    	$scope.pages.push(i);
    }

  }

  var parseSiteFromURL = function (url) {
    var site;
    if (url.indexOf('/docs/') > 0) {
      site = 'Apigee Documentation';
    } else if (url.indexOf('community.apigee.com') > 0) {
      site = 'Apigee Community';
    } else if (url.indexOf('blog.apigee.com') > 0) {
      site = 'Apigee Blog';
    }
    return site;
  }

  var appendSiteToTitle = function (title, site) {
    if (title.match(/\s[\-\|]\sApigee.+$/)) {
      title = title.replace(/\s[\-\|]\sApigee.+$/, ' | ' + site);
    } else if (title.match(/\.\.\.$/)) {
      title = title + ' | ' + site;
    }
    return title;
  }

  var truncateUrl = function (url) {
    if (url.length > 100) {
      url = url.substring(0, 100) + '...';
    }
    return url;
  }
};
