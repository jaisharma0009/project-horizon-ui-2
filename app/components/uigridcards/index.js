/**
 * 
 *  Cards View Directive for uiGrid
 *  Extends the ui grid by adding a cards view option
 *  
 *  - Grid API
 * 
 *    A cards object is added to the gridApi from the grid wich it was attached to
 * 
 *    gridScope.grid.api.cards
 *      @isActive {function} returns true if the cardsView is active
 *      @getTitle {function} returns the text for a toggle tooltip
 *      @toggle {function} enables/disables cards views
 *      @enable {function} enables the cards view
 * 
 *  - Options object
 * 
 *    The directive reads from the grid options object
 *    If a gridName property is added, it will remember enable/disabled state of the cardsView
 *    
 *    A cardIndex property can be added to column definition objects to define the order
 *    in which they will appear in the cards.
 *  
 *    A cardClass property can be added to column definition objects to add custom css.
 *   
 *  - Toggling the view
 * 
 *    The switch of the views can be fired using the gridApi toggle or enable functions 
 *    or using the TOGGLE_VIEW event.
 * 
**/

'use strict';

var $ = require('jquery');
var uuidGen = require('../utils/uuidGen');
var gTemplate = require('./template');
var scrollbar = require("jquery.scrollbar");

module.exports = angular.module('app.components.tsuigridcards', [])
  .directive('uiGridCards', function ($rootScope, $timeout, $compile) {
    return {
      restrict: 'A',
      require: 'uiGrid',
      scope: false,
      link: function(scope, element, attrs, gridScope) {
        let pagination = gridScope.grid.api.pagination;
        let id = uuidGen();
        let type = attrs.uiGridCards || null;
        scope.cardView = false;
        scope.element = element;
        scope.grid = gridScope.grid;

        $rootScope.$watch(() => gridScope.grid.api.core.getVisibleRows().length, 
          (newVal, oldVal) => {
          scope.data = gridScope.grid.api.core.getVisibleRows();

          if(gridScope.grid.options.gridName) {
            let isActive = localStorage.getItem(`cardView.${gridScope.grid.options.gridName}`);
            
            if(JSON.parse(isActive)) {
              gridScope.grid.api.cards.enable();
            }
          }

        }, true);

        $(document).ready(() => {
          $(`#c-${id}`).addClass('custom-grid-scroll');
        })

        //Options from uiGrid directive
        scope.options = gridScope.grid.options;
        let defs = scope.options.columnDefs;
        let template = gTemplate(id, defs, type);
        let parent = $(element).parent();
         
        //Handling columns visibility change
        scope.columnVisibility = [];
        _.each(defs, item => {
          let visible = !!item.visible || item.visible === undefined;
          scope.columnVisibility[item.field] = visible;
        });

        gridScope.grid.api.core.on.columnVisibilityChanged(scope, col => {
          scope.columnVisibility[col.field] = col.visible;
        });

        $rootScope.$on('UI_GRID_COLUMN_CHANGE', (evnt, column) => {
          scope.columnVisibility[column] = !scope.columnVisibility[column];
        })

        //Prepends the template to the parent
        element.append(template);

        //Compiles the added element with scope
        let el = parent.find('#' + id);
        $compile(el.contents())(scope);

        //Adds the card functions in the grid api
        let toggleView = function() {
          this.cardView = !this.cardView;

          if(gridScope.grid.options.gridName) {
            let name = gridScope.grid.options.gridName;
            localStorage.setItem(`cardView.${name}`, this.cardView);
          }

          $(this.element).find('.ui-grid-viewport').toggle();

          if(this.cardView) {
            $(this.element).find('.ui-grid-header-viewport').addClass('scrollable');
          } else {
            $(this.element).find('.ui-grid-header-viewport').removeClass('scrollable');
          }

          resizeGrid();
        }

        let getActive = function() {
          return !!this.cardView;
        }

        let getMessage = function() {
          return !!this.cardView ? 'List view' : 'Cards view';
        }

        let setActiveView = function() {
          this.cardView = true;
          $(this.element).find('.ui-grid-viewport').hide();
          $(this.element).find('.ui-grid-header-viewport').addClass('scrollable');
        }

        gridScope.grid.api.cards = {
          isActive: getActive.bind(scope),
          getTitle: getMessage.bind(scope),
          toggle: toggleView.bind(scope),
          enable: setActiveView.bind(scope)
        }

        scope.$on('TOGGLE_VIEW', (evnt, id) => {
          if(gridScope.grid.id == id) {
            gridScope.grid.api.cards.toggle();
          }
        });

        //Adds the toggle view option to the grid menu
        let gridMenuAdded = false;
        gridScope.grid.api.core.on.rowsRendered(scope, function() {
          if(gridScope.grid.api.core.addToGridMenu && !gridMenuAdded) {
            let menus = [{
              title: "Toggle view",
              action: function($event){
                gridScope.grid.api.cards.toggle();
              },
              icon: "fa fa-th"
            }]

            gridScope.grid.api.core.addToGridMenu(gridScope.grid, menus);
            gridMenuAdded = true;
          }

          resizeGrid();
        });

        function resizeGrid(height) {
          let footerHeigth = $(element).find('.ui-grid-pager-panel').outerHeight() || 0;
          let headerHeight = $(element).find('.ui-grid-header-cell-row').outerHeight() || 0;

          if(!height) {
            height = $('.ui-grid-viewport').height();
          } else {
            height -= headerHeight;
          }
          
          $(`#c-${id}`).height(height);
          $(`#c-${id}`).css('top', headerHeight);
        }

        /** Resize the panel if uigridheight is active **/
        $rootScope.$on('GRID_RESIZED', (evnt, height) => {
          resizeGrid(height);
        });
      }
    }
  });
