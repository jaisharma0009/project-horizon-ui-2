'use strict';

var angular = require('angular');
/**
 * Directive for displaying a sidebar that allows navigation between states
 * for it to work at its best it is recommended that it is placed in the
 * parent view and navigation is between substates
 * 
 * @ngTitle (optional) <String> - the heading the sidebar will show
 * @options <Object>
 *  backLink <Object> (optional)
 *    name <String> display name for the back link
 *    state <String> state wich the back link will redirect to
 *  sections <Array[Object]>
 *    name <String> display name for the nav link
 *    state <String> state wich the nav link will redirect to
 **/
module.exports = angular.module('app.components.sidebar', [])
  .directive('tsSidebar', function() {
    return {
      restrict: 'E',
      scope: {
        options: '=',
        ngTitle: '@'
      },
      transclude: true,
      template: require('./template.html'),
      controller: 'tsSidebarController'
    };
  })
  .controller('tsSidebarController', require('./controller'));
