/**
 * Created by rmahalank on 19/02/17.
 */
/*global angular */

'use strict';

var _ = require('lodash');
var angular = require('angular');
var d3 = require('d3');
var $ = require('jquery');

module.exports = angular.module('app.customers.detail.engagements.radarModule', [])
  .directive('radarDirective', function () {
    return {
      restrict: 'A',
      scope: {
        data: "="
      },
      template: '<div class="radarChart"></div>',
      controller: function ($scope) {
        $scope.$watch("data", function (newValue, oldValue) {
          //Call function to draw the Radar chart
          RadarChart(".radarChart", $scope.data.data, $scope.data.radarChartOptions);
        }, true);


        //By Dave Feuer
        /////////////////////////////////////////////////////////
        /////////////// The Radar Chart Function ////////////////
        /// Based on code written by Nadieh Bremer //////////////
        /////////// Inspired by the code of alangrafu ///////////
        ///////// $Blame Dave Feuer feuerd@google.com ///////////
        /////////////////////////////////////////////////////////

        function RadarChart(id, data, options) {
          var cfg = {
            w: 600,				//Width of the circle
            h: 600,				//Height of the circle
            margin: {top: 20, right: 20, bottom: 20, left: 20}, //The margins of the SVG
            levels: 3,				//How many levels or inner circles should there be drawn
            maxValue: 4, 			//What is the value that the biggest circle will represent
            labelFactor: 1.15, 	//How much farther than the radius of the outer circle should the labels be placed
            wrapWidth: 60, 		//The number of pixels after which a label needs to be given a new line
            opacityArea: 0, 	    //The opacity of the area of the blob  //DEF if you don't want a blob....
            dotRadius: 10, 		//The size of the colored circles of each blob
            opacityCircles: 0.1, 	//The opacity of the circles of each blob
            strokeWidth: 0, 		//The width of the stroke around each blob  //DEF if you don't want connecting lines...
            roundStrokes: false,	//If true the area and stroke will follow a round path (cardinal-closed)
            color: d3.scale.category10()	//Color function - this enables you to switch the colors via CSS if needed
          };

          //Put all of the options into a variable called cfg
          if ('undefined' !== typeof options) {
            for (var i in options) {
              if ('undefined' !== typeof options[i]) {
                cfg[i] = options[i];
              }
            }//for i
          }//if

          //If the supplied maxValue is smaller than the actual one, replace by the max in the data
          var maxValue = Math.max(cfg.maxValue, d3.max(data, function (i) {
            return d3.max(i.map(function (o) {
              return o.value;
            }))
          }));

          var allAxis = (data[0].map(function (i, j) {
              return i.axis
            })),	//Names of each axis
            total = allAxis.length,					//The number of different axes
            radius = Math.min(cfg.w / 2, cfg.h / 2), 	//Radius of the outermost circle
            Format = d3.format(""),                 //Whole number formatting
            angleSlice = Math.PI * 2 / total;		//The width in radians of each "slice"

          //Scale for the radius
          var rScale = d3.scale.linear()
            .range([0, radius])
            .domain([0, maxValue]);

          /////////////////////////////////////////////////////////
          ////////////// Create the container SVG  ////////////////
          /////////////////////////////////////////////////////////

          //Remove whatever chart with the same id/class was present before
          d3.select(id).select("svg").remove();

          //Initiate the radar chart SVG
          var svg = d3.select(id).append("svg")
            .attr("width", cfg.w + cfg.margin.left + cfg.margin.right)
            .attr("height", cfg.h + cfg.margin.top + cfg.margin.bottom)
            .attr("class", "radar" + id);
          //Append a g element
          var g = svg.append("g")
            .attr("transform", "translate(" + (cfg.w / 2 + cfg.margin.left) + "," + (cfg.h / 2 + cfg.margin.top) + ")");

          /////////////////////////////////////////////////////////
          ////////// Glow filter for some extra pizzazz ///////////
          ///// feuerd: unused because we only use the dots ///////
          /////////////////////////////////////////////////////////

          //Filter for the outside glow
          var filter = g.append('defs').append('filter').attr('id', 'glow'),
            feGaussianBlur = filter.append('feGaussianBlur').attr('stdDeviation', '2.5').attr('result', 'coloredBlur'),
            feMerge = filter.append('feMerge'),
            feMergeNode_1 = feMerge.append('feMergeNode').attr('in', 'coloredBlur'),
            feMergeNode_2 = feMerge.append('feMergeNode').attr('in', 'SourceGraphic');

          /////////////////////////////////////////////////////////
          /////////////// Draw the Circular grid //////////////////
          /////////////////////////////////////////////////////////

          //Wrapper for the grid & axes
          var axisGrid = g.append("g").attr("class", "axisWrapper");

          //Draw the background circles
          axisGrid.selectAll(".levels")
            .data(d3.range(1, (cfg.levels + 1)).reverse())
            .enter()
            .append("circle")
            .attr("class", "gridCircle")
            .attr("r", function (d, i) {
              return radius / cfg.levels * d;
            })
            .style("fill", "#CDCDCD")
            .style("stroke", "#CDCDCD")
            .style("fill-opacity", cfg.opacityCircles)
            .style("filter", "url(#glow)");

          //Text indicating what each level is
          axisGrid.selectAll(".axisLabel")
            .data(d3.range(1, (cfg.levels + 1)).reverse())
            .enter().append("text")
            .attr("class", "axisLabel")
            .attr("x", -8) //was 4 => moved to -8
            .attr("y", function (d) {
              return -d * radius / cfg.levels;
            })
            .attr("dy", "0.4em")
            .style("font-size", "10px")
            .attr("fill", "#737373")
            .text(function (d, i) {
              return Format(maxValue * d / cfg.levels);
            });

          /////////////////////////////////////////////////////////
          //////////////////// Draw the axes //////////////////////
          /////////////////////////////////////////////////////////

          //Create the straight lines radiating outward from the center
          var axis = axisGrid.selectAll(".axis")
            .data(allAxis)
            .enter()
            .append("g")
            .attr("class", "axis");
          //Append the lines
          axis.append("line")
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", function (d, i) {
              return rScale(maxValue * 1.1) * Math.cos(angleSlice * i - Math.PI / 2 + Math.PI / 10);
            }) //feuerd: add .314 to "twist" 18 degrees so axis labels are in between each axis
            .attr("y2", function (d, i) {
              return rScale(maxValue * 1.1) * Math.sin(angleSlice * i - Math.PI / 2 + Math.PI / 10);
            }) //feuerd: add .314 to "twist" 18 degrees so axis labels are in between each axis
            .attr("class", "line")
            .style("stroke", "white")
            .style("stroke-width", "2px");

          /*
           //Append the labels at each axis
           var n = rScale(maxValue * cfg.labelFactor) * Math.cos(angleSlice*i - Math.PI/2);
           console.log("maxValue is: ", maxValue);
           console.log("cfg.labelFactor is", cfg.labelFactor)
           console.log("Angleslice is: ", angleSlice);
           console.log("i is: ", i);

           console.log("cfg.labelFactor is", cfg.labelFactor);
           console.log("maxValue is: ", maxValue);
           */

          axis.append("text")
            .attr("class", "legend")
            .style("font-size", "11px")
            .attr("text-anchor", "middle")
            .attr("dy", "0.35em")
            .attr("x", function (d, i) {
              return rScale(maxValue * cfg.labelFactor) * Math.cos(angleSlice * i - Math.PI / 2);
            })
            .attr("y", function (d, i) {
              return rScale(maxValue * cfg.labelFactor) * Math.sin(angleSlice * i - Math.PI / 2);
            })


            .text(function (d) {
              return d
            })
            .call(wrap, cfg.wrapWidth);

          /////////////////////////////////////////////////////////
          ///////////// Draw the radar chart blobs ////////////////
          /////////////////////////////////////////////////////////

          //The radial line function
          var radarLine = d3.svg.line.radial()
            .interpolate("linear-closed")
            .radius(function (d) {
              return rScale(d.value);
            })
            .angle(function (d, i) {
              return i * angleSlice;
            });

          if (cfg.roundStrokes) {
            radarLine.interpolate("cardinal-closed");
          }

          //Create a wrapper for the blobs
          var blobWrapper = g.selectAll(".radarWrapper")
            .data(data)
            .enter().append("g")
            .attr("class", "radarWrapper");

          //Append the backgrounds
          blobWrapper
            .append("path")
            .attr("class", "radarArea")
            .attr("d", function (d, i) {
              return radarLine(d);
            })
            .style("fill", function (d, i) {
              return cfg.color(i);
            })
            .style("fill-opacity", cfg.opacityArea)
            .on('mouseover', function (d, i) {
              //Dim all blobs
              d3.selectAll(".radarArea")
                .transition().duration(200)
                .style("fill-opacity", 0.1);
              //Bring back the hovered over blob
              d3.select(this)
                .transition().duration(200)
                .style("fill-opacity", 0.7);
            })
            .on('mouseout', function () {
              //Bring back all blobs
              d3.selectAll(".radarArea")
                .transition().duration(200)
                .style("fill-opacity", cfg.opacityArea);
            });

          //Create the outlines
          blobWrapper.append("path")
            .attr("class", "radarStroke")
            .attr("d", function (d, i) {
              return radarLine(d);
            })
            .style("stroke-width", cfg.strokeWidth + "px")
            .style("stroke", function (d, i) {
              return cfg.color(i);
            })
            .style("fill", "none")
            .style("filter", "url(#glow)");


          //Append the circles
          blobWrapper.selectAll(".radarCircle")
            .data(function (d, i) {
              return d;
            })
            .enter().append("circle")
            .attr("class", "radarCircle")
            .attr("r", cfg.dotRadius)
            .attr("cx", function (d, i) {
              return rScale(d.value) * Math.cos(angleSlice * i - Math.PI / 2);
            })
            .attr("cy", function (d, i) {
              return rScale(d.value) * Math.sin(angleSlice * i - Math.PI / 2);
            })
            //		.style("fill", function(d,i,j) { console.log("j is: ", j); return cfg.color(j); }

            //Google Red: #EA4335
            //Google Yellow: Hex  #FABB05
            //Google Green: Hex  #34A853
            //Google Blue: Hex #4285F4

            .style("fill", function (d, i, j) {

              var num = Number(d.direction);
              switch (num) {
                case  -1: {
                  return "#EA4335";
                } //Google Red
                  break;
                case 0: {
                  return "FABB05";
                } //Google Yellow
                  break;
                case 1: {
                  return "34A853";
                } //Google Green
                  break;
                default: {
                  return "#4285F4";
                } //default to Blue
              }

              /*
               if Number(d.value) = 0.05 {console.log("d.value == 0.05 cfg.color is: #EA4335"); return cfg.color("#EA4335");}
               if Number(d.value) = 0.25 {console.log("d.value == 0.25 cfg.color is: #FABB05"); return cfg.color("FABB05");}
               if Number(d.value) = 0.50 {console.log("d.value == 0.50 cfg.color is: #FABB05"); return cfg.color("FABB05");}
               if Number(d.value) = 0.75 {console.log("d.value == 0.75 cfg.color is: #34A853"); return cfg.color("34A853");}
               if Number(d.value) = 1 	 {console.log("d.value == 1 cfg.color is: #34A853"); return cfg.color("34A853");}
               */
            })
            .style("fill-opacity", 0.8);


          /////////////////////////////////////////////////////////
          //////// Append invisible circles for tooltip ///////////
          /////////////////////////////////////////////////////////

          //Wrapper for the invisible circles on top
          var blobCircleWrapper = g.selectAll(".radarCircleWrapper")
            .data(data)
            .enter().append("g")
            .attr("class", "radarCircleWrapper");

          //Append a set of invisible circles on top for the mouseover pop-up
          blobCircleWrapper.selectAll(".radarInvisibleCircle")
            .data(function (d, i) { /*console.log("d is", d);*/
              return d;
            })
            .enter().append("circle")
            .attr("class", "radarInvisibleCircle popovertrigger")
            .attr("r", cfg.dotRadius * 1.5)
            .attr("cx", function (d, i) {
              return rScale(d.value) * Math.cos(angleSlice * i - Math.PI / 2);
            })
            .attr("cy", function (d, i) {
              return rScale(d.value) * Math.sin(angleSlice * i - Math.PI / 2);
            })

            .attr('data-toggle', 'popover')
            .attr('title', function (d, i){
              return d.axis;
            })
            .attr('data-content', 'Very engaging internal notes...')
            .attr('data-container', 'body')
            .attr('trigger', 'hover')

            .style("fill", "none")
            .style("pointer-events", "all")
            .on("click", function(d, i){
              alert('TODO: Edit "'+d.axis+'" notes and values');
            });
            // .on("mouseover", function (d, i) {
            //   var newX = parseFloat(d3.select(this).attr('cx')) - 12;
            //   var newY = parseFloat(d3.select(this).attr('cy')) - 12;

            //   var tipText = 'Internal Notes: ';
            //   if(d.internal_notes)
            //     tipText += d.internal_notes;

            //   var tip = tooltip
            //     .attr('x', newX)
            //     .attr('y', newY)
            //     .text(tipText)
            //     .transition().duration(200)
            //     .style('opacity', 1);
            // })
            // .on("mouseout", function () {
            //   tooltip.transition().duration(200)
            //     .style("opacity", 0);
            // });


          //If you want tooltips, here's where you do it...


          //Set up the small tooltip for when you hover over a circle
          var tooltip = g.append("text")
            .attr("class", "tooltip")
            .style("opacity", 0);

          /////////////////////////////////////////////////////////
          /////////////////// Helper Function /////////////////////
          /////////////////////////////////////////////////////////

          //Taken from http://bl.ocks.org/mbostock/7555321
          //Wraps SVG text
          function wrap(text, width) {
            text.each(function () {
              var text = d3.select(this),
                words = text.text().split(/\s+/).reverse(),
                word,
                line = [],
                lineNumber = 0,
                lineHeight = 1.4, // ems
                y = text.attr("y"),
                x = text.attr("x"),
                dy = parseFloat(text.attr("dy")),
                tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");

              while (word = words.pop()) {
                line.push(word);
                tspan.text(line.join(" "));
                if (tspan.node().getComputedTextLength() > width) {
                  line.pop();
                  tspan.text(line.join(" "));
                  line = [word];
                  tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                }
              }
            });
          }//wrap

        }//RadarChart


      }

    };
  });




