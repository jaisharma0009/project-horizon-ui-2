/**
 *
 *  Height fix directive for uiGrid
 *  Sets the uigrid height to always fill the page height
 *
**/

'use strict';

var angular = require('angular');
var uib = require('angular-ui-bootstrap');
var $ = require('jquery');
var paginationPageSizes = require('../../values.js').paginationSizes;

/**
 * Directive for displaying a product name
 */
module.exports = angular.module('app.components.uigridheight', [])
  .directive('uiGridHeight', function ($rootScope, $timeout, $templateCache, $interval) {
    return {
      restrict: 'A',
      require: 'uiGrid',
      scope: false,
      link: function (scope, element, attrs, gridScope) {

        if(!gridScope.grid.data)
          gridScope.grid.data = [];
        
        var gridObject = gridScope.grid;
        var gridEl = $("#"+element[0].id);

        $interval(() => {
          $('.ui-grid-viewport').scrollLeft(0);
        }, 250, 12);

        function getFullHeight(element) {
          var padding = $(element).css('padding');
          var paddingNum = 0;

          if(padding)
            var paddingNum = padding.replace(/(^\d+)(.+$)/i,'$1') * 2;

          return $(element).height() + paddingNum;
        }

        var resizeGrid = function () {
          var pagerHeight = getFullHeight('.ui-grid-pager-panel');
          pagerHeight += 8; //Fix for the scroll bar

          var rowHeight = (getFullHeight('.ui-grid-cell') - 5) || 44;

          var newHeight = $(window).height() - gridEl.offset().top - rowHeight;
          newHeight = newHeight -10; //To prevent second scroll
          newHeight = newHeight - pagerHeight;
          gridEl.height(newHeight + pagerHeight);

          if(newHeight > 180) {
            $('.ui-grid-viewport').height(newHeight - 44);
          }

          //When the grid is resized an event with the height is broadcast
          $rootScope.$broadcast('GRID_RESIZED', newHeight);
        };

        scope.$watch(() => gridObject.data.length, newVal => {
          var sizes = paginationPageSizes.sizes;

          if(newVal > 0) {
            gridObject.paginationPageSize = paginationPageSizes.default;
            sizes.push({label: 'All', value: newVal});
            gridObject.paginationPageSizes = sizes;
          }
        });

        var resizeGridTimeout = function(timeout) {
          $timeout(resizeGrid, timeout || 100);
        }

        scope.$on('STATE_CHANGED', () => { resizeGridTimeout(200) });
        scope.$on('ALERT_REMOVED', resizeGridTimeout);
        scope.$on('RESIZE_WINDOW', resizeGrid);
        scope.$on('TSCOLLAPSE_HIDE', () => { resizeGridTimeout(200) });
        scope.$on('TSCOLLAPSE_SHOW', () => { resizeGridTimeout(200) });
      }

    };
  });
