'use strict';

var angular = require('angular');

/**
 * Directive for displaying a checkmark indicator
 */
module.exports = angular.module('app.components.tscheck', [])
  .directive('tsCheck', function() {
    return {
      restrict: 'E',
      scope: {
        tsChecked: '&'
      },
      template: require('./template.html')
    };
  });
