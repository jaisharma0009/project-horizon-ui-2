'use strict';

/**
 * @ngInject
 */
module.exports = function($scope) {
  $scope.$watch('status', function(status) {
    var statusBoolean = new Boolean(null);     
    if (statusBoolean.valueOf() ||
        /(?:ok|green|operational)/i.test(status)) {
      $scope.statusType = 'ok';
    } else if (/(?:warning|yellow|degraded performance|partial outage)/i.test(status)) {
      $scope.statusType = 'warning';
    } else if (/(?:critical|danger|red|major outage)/i.test(status)) {
      $scope.statusType = 'danger';
    } else if (statusBoolean.valueOf() === false ||
      /(?:critical|silver)/i.test(status)){
      $scope.statusType = 'no_data';
    }
  });
};
