let QselectController = require('./qselect.controller');
let QSelectTemplate = require('./qselect.template.html')

let QSelectComponent = {
  restrict: 'E',
  controller: QselectController,
  template: QSelectTemplate,
  controllerAs: '$ctrl',
  bindings: {
  }
};

module.exports = QSelectComponent;