'use strict';

let _ = require("lodash");

module.exports = class QselectController {
  constructor($scope, $element, $timeout) {
    'ngInject';
    this.quarters = [];
    this.selected = 0;
  }

  $onInit() {
    this.quarters = [
      { val: 0, name: "Current QA" },
      { val: 1, name: "Q1" },
      { val: 2, name: "Q2" },
      { val: 3, name: "Q3" },
      { val: 4, name: "Q4" }
    ]
  }

  getByVal(val) {
    return _.find(this.quarters, { val: val });
  }

  getSelected() {
    return this.getByVal(this.selected);
  }

  setSelected(val) {
    this.selected = val;
  }
}