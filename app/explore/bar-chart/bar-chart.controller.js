'use strict';

let d3 = require('d3');
let $ = require('jquery');
let _ = require('lodash');
let approx = require('approximate-number');

d3.selection.prototype.moveToFront = function () {
  return this.each(function () {
    this.parentNode.appendChild(this);
  });
};

let maxValue = data => {
  let max = [];
  let goals = [];

  _.each(data, item => {
    _.each(item, i => {
      if(!!i.goal) {
        max.push({ value: i.goal*1 });
      }
    })

    max.push(_.maxBy(item, o => (o.value || 1) * 1))
  })


  return _.maxBy(max, o => (o.value || 1) * 1);
}

module.exports = class BarChartController {
  constructor($scope, $element, $timeout) {
    'ngInject';

    this.$element = $element;
    this.$timeout = $timeout;
    this.$scope = $scope;
  }

  $onInit() {
    this.$scope.$watch(() => this.data, newVal => {
      if (!!newVal) {
        this.$timeout(this.initChart.bind(this), 500);
      }
    })
  }

  initChart() {
    $(this.$element).html('');

    //Max maximmum value where linear sizing will apply
    this.linearLimit = 45;

    //Min size of rect
    this.minSize = 20;

    switch (this.chartSize) {
      case 'small':
        this.height = 170,
        this.barHeight = 143,
        this.barWidth = 46;
        this.paddingBottom = 20;
        this.textPos = 10;
        this.labelTopOffset = 12;
        this.axisCorrection = -20;
        break;

      case 'med':
        this.height = 255,
        this.barHeight = 227,
        this.barWidth = 50;
        this.paddingBottom = 15;
        this.textPos = 15;
        this.labelTopOffset = 6;
        this.axisCorrection = -15;
        break;

      default:
        this.height = 315,
        this.barHeight = 272,
        this.barWidth = 85;
        this.paddingBottom = 0;
        this.textPos = 15;
        this.labelTopOffset = 12;
        this.axisCorrection = 0;
        break;
    }

    if (this.type == 'blue-chart') {
      this.labelTopOffset = 0;
    }

    this.axisClass = !!this.axis ? 'axis' : '';

    //Resize scrollable chart container
    if (!!this.class) {
      if (this.class.indexOf('scrollable-chart') != -1) {
        let aw = !!this.axis ? 5 : 0;
        $(this.$element).width(Object.keys(this.data).length * ((this.barWidth + aw) * 2 + 20));
      }
    }

    //Gets the max posible value in all of the charts
    let maxVal = !!maxValue(this.data) ? maxValue(this.data).value : 1;

    //Generates the svg element for each of the charts
    _.each(this.data, (item, index) => {
      if (!!item[0].value && !!item[1].value) {

        let svg = `<svg id="${this.chartId}-${index}" style="margin-top: ${this.axisCorrection * -1}"
                      class="chart ${this.type} ${this.axisClass} ${this.chartId}" 
                      width="${(this.barWidth * 2)}" 
                      height="272">
                 </svg>`;

        $(this.$element).append(svg);

        //In case the name comes with _ to avoid problems with spaces 
        //because it is used for bold labels
        let name = index.split('_').join(' ');

        //Generates each chart
        this.generateChart(item, `#${this.chartId}-${index}`, name, maxVal);
      }
    });

    //Hover behaviour for charts
    let charts = $(`.${this.chartId}`);
    $(charts).mouseenter(event => {
      $(charts).addClass('disabled');
      $(event.currentTarget).removeClass('disabled');
    });

    $(charts).mouseleave(event => {
      $(charts).removeClass('disabled');
    });
  }

  generateChart(data, selector, name, max) {
    let height = this.height,
      barHeight = this.barHeight,
      barWidth = this.barWidth,
      paddingBottom = this.paddingBottom,
      labelTopOffset = this.labelTopOffset;

    //Scaling function for the bars
    let y = this.sizingFunction(max, barHeight, 0, 0, this.linearLimit);

    var chart = d3.select(selector)
      .attr("height", height);

    var bar = chart
      .selectAll("g")
      .data(data)
      .enter().append("g")
      .attr("class", (d, i) => `g-${i}`)
      .attr("transform", function (d, i) {
        let top = barHeight - (y(d.value)) + paddingBottom;
        let left = i * barWidth;

        return `translate(${left}, ${top})`;
      })
      .attr("width", barWidth);

    let rect = bar.append("rect")
      .attr("class", d => {
        let other = d.value == 0 ? 'grey' : '';
        return d.type + ' ' + other;
      })
      .attr("height", d => y(d.value) > 0 ? y(d.value) : 2)
      .attr("width", barWidth)
      .attr("y", d => {
        if(y(d.value) == 0 && !!this.axisCorrection) {
          return this.axisCorrection - 1;
        }

        return this.axisCorrection
      })

    rect.append("animate")
      .attr("attributeType", "XML")
      .attr("attributeName", "height")
      .attr("from", "0")
      .attr("to", d => y(d.value))
      .attr("dur", "0.5s")

    rect.append("animate")
      .attr("attributeType", "XML")
      .attr("attributeName", "y")
      .attr("to", this.axisCorrection)
      .attr("from", d => y(d.value) + this.axisCorrection)
      .attr("dur", "0.5s")

      bar.append("line")
      .attr("class", d => !!d.goal ? 'goal' : 'hidden')
      .attr("stroke-dasharray", "5,5")
      .attr("x1", 0)
      .attr("x2", barWidth)
      .attr("y1", d => {
        if (!!d.goal) {
          return -1 * (y(d.goal) - y(d.value));
        } else {
          return null;
        }
      })
      .attr("y2", d => {
        if (!!d.goal) {
          return -1 * (y(d.goal) - y(d.value));
        } else {
          return null;
        }
      })

    bar.append("text")
      .attr("width", barWidth)
      .attr("class", d => {
        let hidden = "";
        let other = "";

        if(y(d.value*1) < this.minSize) {
          other = 'grey-text';
        }

        return `${d.type}-text ${hidden} ${other}`;
      })
      .attr("text-anchor", "middle")
      .attr("x", barWidth / 2)
      .attr("y", d => {
        let val = y(d.value);
        if(val < this.minSize && val != 0) {
          return -5;
        }

        let h = barHeight - (y(d.value));
        let hh = barHeight - 10 - h + this.axisCorrection;
        return hh;
      })
      .text(d => {
        return approx(d.value, { min10k: true });
      });

    bar.append("text")
      .attr("class", "subtitle-text")
      .attr("width", barWidth)
      .attr("text-anchor", "middle")
      .attr("x", barWidth / 2)
      .attr("y", d => {
        let h = barHeight - (y(d.value));
        return barHeight + 15 - h;
      })
      .text(d => d.label);

    

    bar.append("rect")
      .attr("class", d => !!d.goal ? "goal-tooltip" : "hidden")
      .attr("width", 68)
      .attr('height', 33)
      .attr("x", barWidth + 2)
      .attr("y", d => !!d.goal ? -1 * (y(d.goal) - y(d.value)) - 33 : null)

    bar.append("text")
      .attr("class", d => !!d.goal ? "goal-tooltip-title" : "hidden")
      .text('Goal:')
      .attr("x", barWidth + 5)
      .attr("y", d => !!d.goal ? -1 * (y(d.goal) - y(d.value)) - 20 : null)

    bar.append("text")
      .attr("class", d => !!d.goal ? "goal-tooltip-text" : "hidden")
      .text(d => approx(d.goal, { min10k: true }))
      .attr("x", barWidth + 5)
      .attr("y", d => !!d.goal ? -1 * (y(d.goal) - y(d.value)) - 5 : null)

    chart.append("text")
      .attr("class", "title-text")
      .attr("width", barWidth)
      .attr("text-anchor", "middle")
      .attr("x", barWidth)
      .attr("y", height - labelTopOffset)
      .text(name);

    if (this.axis) {
      let yRange = this.sizingFunction(max, 0, barHeight, 0, this.linearLimit);

      let yAxis = d3.svg
        .axis()
        .scale(yRange)
        .orient('left')
        .ticks(6)
        .tickFormat((d, index) => approx(d, { min10k: true }));

      chart.call(yAxis);
    }

    d3.select(`${selector} .g-0`).moveToFront();

  }

  capitalize(sstring) {
    return string[0].toUpperCase() + string.slice(1);
  }

  sizingFunction(valTo, sizeTo, sizeFrom = 0, valFrom = 0, linearLimit = 45) {
    let func = d3.scale.sqrt().domain([valFrom, valTo]).range([sizeFrom, sizeTo]);

    if (valTo <= linearLimit) {
      func = d3.scale.linear().domain([valFrom, valTo]).range([sizeFrom, sizeTo]);
    }

    return func;
  }
}
