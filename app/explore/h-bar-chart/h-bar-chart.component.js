var HBarChartController = require('./h-bar-chart.controller');

let HBarChartComponent = {
  restrict: 'E',
  controller: HBarChartController,
  controllerAs: '$ctrl',
  bindings: {
    chartSize: '@',
    chartId: '@',
    type: '@',
    axis: '@',
    data: '='
  }
};

module.exports = HBarChartComponent;