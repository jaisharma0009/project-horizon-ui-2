var template = require('./line-chart.template.html');
var LineChartController = require('./line-chart.controller');

let LineChartComponent = {
  restrict: 'E',
  template,
  controller: LineChartController,
  controllerAs: '$ctrl',
  bindings: {
    type: '@',
    chartId: '@',
    data: '='
  }
};

module.exports = LineChartComponent;
