'use strict';

let d3 = require('d3');
let $ = require('jquery');
let _ = require('lodash');
let approx = require('approximate-number');

module.exports = class LineChartController {
  margin = { top: 30, right: 20, bottom: 30, left: 50 };

  svgWidth = 600;

  svgHeight = 270;

  width = this.svgWidth - this.margin.left - this.margin.right;

  height = this.svgHeight - this.margin.top - this.margin.bottom;

  colors = {
    blue: '#33c4df',
    red: '#fc4c02'
  };

  constructor($element, $scope, $timeout) {
    'ngInject';

    this.$element = $element;
    this.$scope = $scope;
    this.$timeout = $timeout;
  }

  $onInit() {
    let resizeId;
    $(window).resize(() => {
      this.$timeout.cancel(resizeId);
      resizeId = this.$timeout(doneResizing.bind(this), 500);
    });

    function doneResizing() {
      let width = $(this.$element).width();

      this.$element.find('svg').remove();

      this.$timeout(() => {
        this.setDimensions(Math.min(width, 600)).initChart();
      }, 250);
    }

    this.$scope.$watch(() => this.data, newVal => {
      let width = $(this.$element).width();

      if(!!newVal) {
        this.setDimensions(Math.min(width, 600)).initChart();
      }
    });
  }

  setDimensions(width = 600, height = 270) {
    this.margin = { top: 30, right: 20, bottom: 30, left: 50 };

    this.svgWidth = width;

    this.svgHeight = height;

    this.width = this.svgWidth - this.margin.left - this.margin.right;

    this.height = this.svgHeight - this.margin.top - this.margin.bottom;

    return this;
  }

  initChart() {
    $(this.$element).find(`#${this.chartId}-1`).remove();
    $(this.$element).append(`
      <svg id="${this.chartId}-1" class="chart" width="${ this.svgWidth }" height="${ this.svgHeight }"></svg>
    `);

    this.setRanges();
    this.setAxes();
    this.generateChart();
  }

  setRanges() {
    this.yRange = d3.scale.linear().range([this.height, 0], 0.5);
    this.xRange = d3.scale.linear().range([0, this.width]);
  }

  setAxes() {
    this.xAxis = d3.svg.axis().scale(this.xRange)
      .orient('bottom')
      .ticks(5)
      .tickFormat((d, index) => (index >= 1 && index <= 4) ? `Q${ index }` : '');

    this.yAxis = d3.svg.axis().scale(this.yRange)
      .orient('left').ticks(10)// .outerTickSize(0);
  }

  getData() {
    if (this.data.length === 4) {
      this.data.unshift({ currentYear:0, priorYear: 0 });
    }

    this.data.forEach((d) => {
      // d.date = d3.time.format("%d-%b-%y").parse(d.date);
      d.currentYear = +d.currentYear;
      d.priorYear = +d.priorYear;
    });

    return this.data;
  }

  addLineChart(key, data) {
    let valueline = d3.svg.line()
        // .interpolate('basis')
        .x((d, index) => this.xRange(index /*d.date*/))
        .y((d) => this.yRange(d[key]))
      , color = this.getColor(key);

    const addLines = (data, color, strokeWidth = 2, animated = false) => {
      let paths = this.svg.append('path')
          .attr('class', 'line')
          .style('stroke', color)
          .attr('fill', 'none')
          .style('stroke-width', strokeWidth)
          .attr('d', valueline(data));

      if (animated) {
        paths
          .attr('stroke-dasharray', function (d) { return this.getTotalLength(); })
          .attr('stroke-dashoffset', function (d) { return this.getTotalLength(); })
          .transition().duration(1000).ease('linear')
          .attr('stroke-dashoffset', 0);
      }
    };

    let div = d3.select('.line-chart-tooltips').append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);

    const addDots = (data, props = {}) => {
      let g = this.svg.append('g').selectAll('.dot').data(data).enter()

      let dots = g.append('circle')
          .attr('class', 'dot')
          .attr('cx', (d, index) => this.xRange(index /*d.date*/))
          .attr('cy', (d) => this.yRange(d[key]))
          .attr('fill', props.fill || 'white')
          .attr('r', 0)
          .attr('stroke-width', props.strokeWidth || 2)
          .attr('stroke', props.stroke || 'white' );

          
      if(props.labels) {
        setTimeout(() => {
          let label = g.append('text')
          .text((d, index) => index > 0 ? d[key] : '' )
          .attr('text-anchor', 'middle')
          .attr('x', (d, index) => this.xRange(index))
          .attr('y', (d) => this.yRange(d[key]) - 15);

          label.append("animate")
            .attr("attributeType", "XML")
            .attr("attributeName", "opacity")
            .attr("to", 1)
            .attr("from", 0)
            .attr("dur", "2s")
        }, 1000);
      }

      if (props.tooltips) {
        dots.on('mouseover', function (d, index) {
          let posX = this.cx.baseVal.value    // d3.event.pageX
            , posY = this.cy.baseVal.value;   // d3.event.pageY

          div.transition().duration(200).style('opacity', 1);
          div.html(d[key])
            .style('transform', `translate(${ posX + 21 }px, ${ posY - 15}px)`);
        }).on('mouseout', (d) => {
          div.transition().duration(500).style("opacity", 0);
        });
      }

      dots.transition().duration(1000).ease('linear')
        .attr('r', props.radius || 5 )
    };

    addLines(data, color, 2, true);
    addLines(data.slice(0, 2), 'white', 5);
    addDots(data, { radius: 5, stroke: (d, i) => 0 === i ? 'transparent' : color });
    addDots(data, { radius: 7, fill: 'transparent', labels: true });
  }

  getColor(key) {
    return 'currentYear' === key ? this.colors.blue : (
      'priorYear' === key ? this.colors.red : '#555'
    );
  }

  generateChart(selector) {
    this.data = this.getData();

    // Adds the svg canvas
    this.svg = d3.select(this.$element[0]).select('svg')
      .append('svg')
        .attr('width', this.width + this.margin.left + this.margin.right)
        .attr('height', this.height + this.margin.top + this.margin.bottom)
      .append('g')
        .attr('transform', `translate(${ this.margin.left}, ${ this.margin.top })`);

    // Scale the range of the data
    this.xRange.domain([0, this.data.length]);
    this.yRange.domain([0, d3.max(this.data, function(d) { return d.currentYear; })]);

    this.addLineChart('priorYear', this.data);
    this.addLineChart('currentYear', this.data);

    // Add the X Axis
    this.svg.append('g')
      .attr('class', 'axis x-axis')
      .attr('transform', `translate(0, ${ this.height })`)
      .call(this.xAxis);

    // Add the Y Axis
    this.svg.append('g')
      .attr('class', 'axis y-axis')
      .call(this.yAxis);
  }
}
