import angular   from 'angular';
import register  from '../components/directive.register';
import { ScrollbarDirective } from './scrollbar.directive';
import {
  ClassOnStateDirective
}                from './class-on.directive';


let Directives = angular.module('horizon.directives', [])
  .directive('classOnState', register(ClassOnStateDirective))
  .directive('scrollbar', register(ScrollbarDirective))
  .name;

export default Directives;
