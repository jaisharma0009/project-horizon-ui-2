import { val } from '@uirouter/angularjs';

let isNumber = require('is-number');
let solve = require('logician');
let approx = require('approximate-number');
let _ = require('lodash');
let htmlEntities = require('html-entities');

let myNumberFilter = () => {
  return (searchTerm, cellValue) => {
    if(cellValue == '*') return true;

    // Custom logic that returns true if `row` passes the filter
    //and false if it should be filtered out
    if (searchTerm == null) return true;
    if (cellValue == null) return false;

    if (isNumber(searchTerm))
      return searchTerm == cellValue

    cellValue = parseFloat(cellValue);
    searchTerm = searchTerm.replace('\\', '');

    return solve(cellValue + ' ' + searchTerm);
  }
}

let myTextNullFilter = () => {
  return (searchTerm, cellValue) => {
    if(cellValue == '*') return true;

    if (searchTerm == null) return true;

    if (searchTerm == '!null') {
      if (cellValue == null) return false;
      return true;
    }

    if (searchTerm == 'null') {
      if (cellValue == null) return true;
      return false;
    }

    if (cellValue == null) return false;

    searchTerm = searchTerm.toLowerCase();
    cellValue = cellValue.toLowerCase();
    if (searchTerm.charAt(0) != '!') {
      if (cellValue.indexOf(searchTerm) != -1) return true;
    } else {
      if (cellValue.indexOf(searchTerm.substring(1)) < 0) return true;
    }

    return false;
  }
}

let technicalCompetencyFilter = (TECHNICAL_COMPETENCY_RATINGS) => {
  'ngInject';

  return (searchTerm, cellValue) => {
    if (searchTerm == null) return true;
    if (cellValue == null) return false;

    if (isNumber(searchTerm)) {
      return searchTerm == cellValue
    } else {
      let ratingNumber = _.findIndex(TECHNICAL_COMPETENCY_RATINGS, item => {
        let term = searchTerm.toLowerCase();
        let search = item.toLowerCase();

        return search.indexOf(term) != -1;
      });

      return ratingNumber == cellValue;
    }
  }
}

let stageFilter = () => {
  return (value, size) => {
    if(value==null) return "";

    if(size == 'mini') {
      if(value.indexOf(':') == -1) return value;

      let first = value.split(':')[0];
      return first.replace('Stage', '');
    }

    if(size == 'short')
      return value.indexOf(':') != -1 ? value.split(':')[0] : value;

    if(size == 'long')
      return value.indexOf(':') != -1 ? value.split(':')[1] : value;

    return value;
  }
}

let approximate = () => val => {
  if(val == null) return "";

  val = val.toString();
  return approx(val.replace(/,/g, ''), {min10k: true});
}

let noneDash = () => val => {
  if(val == null) return "";

  return val.toLowerCase() == 'none' ? '-' : val;
}

let engageStatusClass = () => val => {
  if(val == null) return "";

  let eClass = 'engagementStatus ';

  switch(val.toLowerCase().trim()) {
    case 'hot':
    case 'high':
      eClass += 'hot';
    break;

    case 'engaged':
    case 'med':
      eClass += 'engaged';
    break;

    case 'cold':
    case 'low':
      eClass += 'cold';
    break;
  }

  return eClass;
}

let engageStatusDotsClass = () => val => {
  if(val == null) return "";

  let eClass = 'icon-detail large-icon ';

  switch(val.toLowerCase().trim()) {
    case 'hot':
    case 'high':
      eClass += 'icon-detail--dot-high';
    break;

    case 'engaged':
    case 'med':
      eClass += 'icon-detail--dot-med';
    break;

    case 'cold':
    case 'low':
      eClass += 'icon-detail--dot-low';
    break;
  }

  return eClass;
}

let quarterFilter = ($sce) => {
  'ngInject';

  return (value) => {
    if(value==null) return "";

    let valArr = value.split(' ');
    let el = `<span>${valArr[1]}'${valArr[0]}</span>`;

    if(!valArr[0]) {
      el = `<span>${value}</span>`;
    }

    return $sce.trustAs('html', el);
  }
}

let hostnameFilter = () => {
  return url => {
    if(url === undefined) return "";

    let hostname;

    //find & remove protocol (http, ftp, etc.) and get hostname
    if (url.indexOf("://") > -1) {
       hostname = url.split('/')[2];
    }
    else {
      hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
  }
}

let htmldecode = () => value => {
  let decoded = htmlEntities.AllHtmlEntities.decode(value);
  return decodeURI(decoded);
}

module.exports = {
  myNumberFilter,
  myTextNullFilter,
  technicalCompetencyFilter,
  stageFilter,
  approximate,
  noneDash,
  engageStatusClass,
  engageStatusDotsClass,
  quarterFilter,
  hostnameFilter,
  htmldecode
};
