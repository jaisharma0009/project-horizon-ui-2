'use strict';

var expect = chai.expect,
  angular = require('angular'),
  angular_mock = require('angular-mocks'),
  app = require('../app'),
  app_test = require('./app'),
  list_test = require('./list/list'),
  kanban = require('./kanban/kanban');

describe('app', app_test);
describe('kanban', kanban);
describe('list', list_test);
