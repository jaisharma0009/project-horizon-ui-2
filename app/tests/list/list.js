var angular_mock = require('angular-mocks'),
  context_mock = require('../mocks/context.mock'),
  $ = require('jquery');

module.exports = function () {
  beforeEach(angular.mock.module('app'));
  beforeEach(angular.mock.module('app.customers.list'));

  var ctrl, element, scope;

  beforeEach(angular_mock.inject(function ($controller, $rootScope, $componentController, $compile) {
    scope = $rootScope.$new();
    element = angular.element('<list></list>');
    element = $compile(element)(scope);

    ctrl = $componentController('list', {$scope: scope});    
    ctrl.contextService = context_mock;

    scope.$digest();
  }));

  it('controller should exist', function () {
    expect(ctrl).to.exist;
  });

  it('should load the list template', function () {
    expect(element.find('h1').text()).to.equals('Account Selection:');
  });

  it('should have scope variables', function () {
    expect(ctrl.scope.visibleTable).to.exist;
    expect(ctrl.scope.visibleTable).to.be.true;
    expect(ctrl.scope.jqSparklineOptions).to.exist;
    expect(ctrl.scope.visibleGridDropdown).to.exist;
  });

  it('should show the dropdown', function () {
    ctrl.scope.visibleGridDropdown.visible = true;
    ctrl.hideGridDrop();
    expect(ctrl.scope.visibleGridDropdown.visible).to.be.false;
  });

  it('myNumberFilter should work', function () {
    expect(ctrl.myNumberFilter('test', null)).to.be.false;
    expect(ctrl.myNumberFilter(null, 'test')).to.be.true;
    expect(ctrl.myNumberFilter(9, 5)).to.be.false;
    expect(ctrl.myNumberFilter(5, 5)).to.be.true;
    expect(ctrl.myNumberFilter('5', 3)).to.be.false;
    expect(ctrl.myNumberFilter('5', 5)).to.be.true;
    expect(ctrl.myNumberFilter(5, '5')).to.be.true;
    expect(ctrl.myNumberFilter('5', '5')).to.be.true;
  });

  if('myTextNullFilter should work', function() {
    expect(ctrl.myTextNullFilter(null)).to.be.true;
    expect(ctrl.myTextNullFilter('test', 'test')).to.be.true;
    expect(ctrl.myTextNullFilter('Upset', 'upset')).to.be.true;
    expect(ctrl.myTextNullFilter('upset', 'Upset')).to.be.true;
    expect(ctrl.myTextNullFilter('upset', 'differet')).to.be.false;
  });

  it('should load customers data', function (done) {
    this.timeout(10000);

    expect(ctrl.scope.gridAccounts.data.length).to.equals(0);
    ctrl.reloadCustomerGrid();
    expect(ctrl.scope.customersTracker.active()).to.be.true;

    setTimeout(function () {
      try {
        expect(ctrl.scope.gridAccounts.data).to.be.an('array');
        //expect(ctrl.scope.gridAccounts.data.length).to.equals(2);

        done();
      } catch (e) { done(e); }
    }, 100);

  });

  it('should refresh filter', function(done) {
    this.timeout(10000);

    expect(ctrl.scope.gridAccounts.data.length).to.equals(0);
    scope.filters.selectedTeam = 'EMEA';
    scope.$apply();

    setTimeout(function () {
      try {
        expect(ctrl.scope.gridAccounts.data).to.be.an('array');
        //expect(ctrl.scope.gridAccounts.data.length).to.equals(2);

        done();
      } catch (e) { done(e); }
    }, 500);
  });

}
