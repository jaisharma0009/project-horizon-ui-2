var template = require('./template.html');
var KanbanController = require('./controller');

let KanbanComponent = {
  restrict: 'E',
  template,
  controller: KanbanController,
  controllerAs: 'kanban'
};

module.exports = KanbanComponent;