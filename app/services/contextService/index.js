'use strict';

var angular = require('angular');

module.exports = angular.module('app.customers.services.contextService', [])
  .service('contextService', require('./service'));