'use strict';

var moment = require('moment'),
  _ = require('lodash');

var poroduct_plan_attributes = [
  'pci',
  'hipaa',
  'named_tss',
  'rps_limit',
  'proxy_limit',
  'phone_support',
  'product_addons',
  'legacy_mp_limit',
  'livechat_access',
  'orgs_envs_limit',
  'p1_response_goal',
  'p2_response_goal',
  'p3_response_goal',
  'p4_response_goal',
  'quarterly_review',
  'number_of_regions',
  'api_calls_per_term',
  'p1_resolution_goal',
  'p2_resolution_goal',
  'p3_resolution_goal',
  'p4_resolution_goal',
  'sla_service_target',
  'support_escalation',
  'service_request_goal',
  'analytics_concurrency',
  'number_of_dev_portals',
  'support_portal_access',
  'analytics_capabilities',
  'collaborative_chatroom',
  'runtime_data_retention',
  'analytics_data_retention',
  'community_support_access',
  'runtime_data_capabilities',
  'product_feature_exceptions',
  'support_feature_exceptions'
];

/**
 * @ngInject
 */
module.exports = function ($stateParams, $timeout, Restangular, csRestangular, promiseTracker, $q, $rootScope) {
  var self = this;

  this.getOpportunityList = function (accountId) {

    var nintey_days_ago = moment().subtract(90, 'days');

    var promise = Restangular
      .all('sfdc/opportunities')
      .getList({
        where: [
          'account_id:eq:' + accountId
        ],
        orderby: 'created_date:desc'
      })
      ;

    return promise;

  };


  this.getApiProxyRevisionVersions = function (account_id, org, api, is_latest_revision, is_latest_version) {

    var where = [];

    if (account_id != null) {
      where.push('account_id:eq:' + account_id);
    }
    if (org != null) {
      where.push('org_name:eq:' + org);
    }
    if (api != null) {
      where.push('api_name:eq:' + api);
    }

    if (is_latest_revision) {
      where.push('is_latest_revision:eq:true');
    }

    if (is_latest_version) {
      where.push('is_latest_version:eq:true');
    }

    return Restangular
      .all('edge/org_api_revision_versions')
      .getList({
        where: where,
        column: [
          'id',
          'org_name',
          'api_name',
          'revision',
          'account_id'
        ],
        orderby: 'api_name:asc'
      });

  }

  this.getApiBundleLinterResults = function (account_id, org, api, revision, latest_only) {

    var where = [];

    if (account_id != null) {
      where.push('account_id:eq:' + account_id);
    }
    if (org != null) {
      where.push('org_name:eq:' + org);
    }
    if (api != null) {
      where.push('api_name:eq:' + api);
    }

    var query = {
      where: where,
      orderby: 'last_update:desc'
    }

    if (latest_only) {
      query.limit = 1;
    }

    return Restangular
      .all('edge/org_api_revision_bundlelints')
      .getList(query)
      ;

  }

  this.getApiProxyList = function (accountId) {
    let where = [
      'account_id:eq:' + accountId,
      'is_latest_revision:eq:true',
      'is_latest_version:eq:true'
    ];

    if(ENV == 'local') {
      where = where = ['account_id:eq:' + accountId];
    }

    return Restangular
      .all('edge/org_api_revision_versions')
      .getList({
        where: where,
        limit: 5000,
        column: [
          'id',
          'org_name',
          'api_name',
          'revision',
          'version',
          'created_date',
          'last_modified_date',
          'account_id'
        ],
        orderby: 'api_name:asc'
      });

  };

  this.getSfdcContactList = function (accountId) {

    return Restangular
      .all('sfdc/contacts')
      .getList({
        where: ['account_id:eq:' + accountId],
        column: [
          'id',
          'name_last',
          'name_first',
          'account_id'
        ],
        orderby: ['name_last:asc', 'name_first:asc']
      })
      ;

  };

  this.getPurchaseProductList = function (accountId) {

    return Restangular
      .all('netsuite/line_items')
      .getList({
        where: [
          'account_id:eq:' + accountId,
          'product_type:notlike:%Service%'
        ],
        orderby: 'order_date:desc'
      });

  }

  this.getPurchaseServiceList = function (accountId) {

    return Restangular
      .all('netsuite/line_items')
      .getList({
        where: [
          'account_id:eq:' + accountId,
          'product_type:like:%Service%'
        ],
        orderby: 'order_date:desc'
      });

  }

  this.getMagellanDimensionScores = function (magellan_instance_id) {

    return Restangular
      .all('sfdc/dimension_scores')
      .getList({
        where: [
          'journey_map_instance_id:eq:' + magellan_instance_id
        ],
        orderby: ['createddate:desc']
      });

  }

  this.getMagellanInstances = function (account_id) {

    return Restangular
      .all('sfdc/account_journey_maps')
      .getList({
        where: [
          'account_id:eq:' + account_id
        ],
        orderby: ['createddate:desc']
      });

  }

  this.getApiProductTraffic = function (account_id) {

    return Restangular
      .all('traffic/apiproduct_traffic')
      .getList({
        where: [
          'account_id:eq:' + account_id,
          'apiproduct:ne:(not set)'
        ],
        orderby: ['org_name:asc', 'apiproduct:asc', 'date:asc'],
        limit: 90
      });

  }

  this.getApiProductList = function (accountId) {

    var promises = [];

    promises.push(Restangular
      .all('edge/org_apiproducts')
      .getList({
        where: 'account_id:eq:' + accountId,
        orderby: 'displayname:asc'
      }));

    promises.push(this.getApiProductTraffic(accountId));

    return $q.all(promises)
      .then(results => {
        var products = results[0];
        var traffic = results[1];
        var traffic_sparklines = {};

        _.each(traffic, item => {
          item.key = item.org_name + '||' + item.apiproduct;
        })
        var org_product_traffic = _.groupBy(traffic, 'key');
        var start_date = moment().subtract(90, 'days');

        _.each(org_product_traffic, org_product => {
          //filter out data older than 90 days
          org_product = _.filter(org_product, item => {
            return start_date.isSameOrBefore(item.date)
          });
          if (org_product.length > 0) {
            var org_product_key = org_product[0].org_name + '||' + org_product[0].apiproduct;
            var org_product_sparkline = [];
            for (var d = moment(start_date); d.isBefore(moment()); d.add(1, 'days')) {
              var match = d.format('YYYY-MM-DD');
              var traffic = 0;
              org_product = _.filter(org_product, item => {
                var this_date = moment(item.date).format('YYYY-MM-DD');
                if (match == this_date) {
                  traffic += item.message_count ? parseInt(item.message_count, 10) : 0;
                }
                return d.isSameOrBefore(item.date);
              });
              org_product_sparkline.push(traffic);
            }
            traffic_sparklines[org_product_key] = org_product_sparkline.join(',');
          }
        });

        _.each(products, product => {
          product.key = product.org_name + '||' + product.displayname;
          // debugger;
          if (traffic_sparklines.hasOwnProperty(product.key)) {
            product.traffic_sparkline = traffic_sparklines[product.key];
          }
        });

        return products;

      })

  };


  this.getAccount = function (account_id) {
    // var endpoint = ENV == 'local' ? 'cs_account_plannings' : 'accounts';

    return Restangular
      .all('sfdc')
      .one('cs_account_plannings', account_id)
      .get()
      .then(account => {
        if (account != undefined) {
          return account;
        }
        return Restangular
          .all('sfdc')
          .one('accounts', account_id)
          .get()
          .then(account => {
            return account;
          });
      })
      .catch(function (err) {
        return Restangular
          .all('sfdc')
          .one('accounts', account_id)
          .get()
          .then(account => {
            return account;
          });
      });
  }

  this.getProductPlan = function (product_plan_id) {

    return Restangular
      .one('sfdc/product_plans', product_plan_id)
      .get()
      .then(plan => {
        return plan;
      });

  }

  this.getAccountEntitlement = function (account_entitlement_id, overlay_on_plan) {

    return Restangular
      .one('sfdc/account_entitlements', account_entitlement_id)
      .get()
      .then(entitlement => {

        entitlement.attributes = poroduct_plan_attributes;

        if (!overlay_on_plan) {
          return entitlement;
        } else {
          return this.getProductPlan(entitlement.product_plan_id)
            .then(plan => {
              _.each(poroduct_plan_attributes, attribute => {
                if (entitlement[attribute] === null) {
                  entitlement[attribute] = plan[attribute];
                }
              });
              return entitlement;
            });
        }
      })
  };

  this.getAccountPlan = function (account_id) {

    return Restangular
      .all('sfdc')
      .all('account_plans')
      .getList({
        where: ['account_id:eq:' + account_id]
      })
      .then(plans => {
        if (Array.isArray(plans) && plans.length > 0) {
          return plans[0];
        } else {
          return this.createAccountPlan(account_id);
        }
      })
      .catch(err => {
        return this.createAccountPlan(account_id);
      });
  };

  this.createAccountPlan = function (account_id) {

    return this.getAccount(account_id)
      .then(account => {

        return csRestangular
          .all('sfdc')
          .all('account_plans')
          .post({
            Account__c: account.id,
            Name: account.account_name + ' Expansion Plan'
          })
          .then(plan => {
            return plan;
          })
          .catch(err => {
            console.error(err);
            throw Error('Error creating Account Plan!');
          })
      })
      .catch(err => {
        console.error(err);
        throw Error('Error retrieving account: ' + account_id);
      });

  };

  this.updateAccountPlan = function (old_plan, new_plan) {

    var fields = {
      "name": "Name",
      "cs_owner": "CS_Owner__c",
      "account_id": "Account__c",
      "expansion_target": "Expansion_Target__c",
      "plan": "Plan__c"
    };

    var obj = {}
    var updated = false;
    _.each(fields, (field, key) => {
      if (old_plan[key] != new_plan[key]) {
        updated = true;
        if (new_plan[key] == null) {
          obj[field] = 'NULL';
        } else {
          obj[field] = new_plan[key];
        }
      }
    });

    if (!updated) {
      return new Promise((resolve, reject) => {
        reject("Nothing to update");
      });
    }

    return csRestangular
      .one('sfdc/account_plans', old_plan.id)
      .put(obj)
      .then(results => {
      })
      .catch(err => {
        console.error(err);
      });

  };

  this.updateExpansionRating = function (id, field, value) {

    var expansion_fields = {
      "name": "Name",
      "budget": "Budget__c",
      "account": "Account__c",
      "priority": "Priority__c",
      "cs_lead_id": "CS_Lead__c",
      "api_count": "API_Count__c",
      "etp_level": "ETP_Level__c",
      "gcp_growth": "GCP_Growth__c",
      "plan_month": "Plan_Month__c",
      "api_traffic": "API_Traffic__c",
      "deal_timing": "Deal_Timing__c",
      "api_dev_count": "API_Dev_Count__c",
      "license_value": "License_Value__c",
      "priority_score": "Priority_Score__c",
      "tech_capability": "Tech_Capability__c",
      "sales_engagement": "Sales_Engagement__c",
      "new_license_value": "New_License_Value__c",
      "executive_commitment": "Executive_Commitment__c",
      "magellan_vision_score": "Magellan_Vision_Score__c",
      "expansion_rating_model_id": "Expansion_Rating_Model__c",
      "magellan_alignment_score": "Magellan_Alignment_Score__c",
      "magellan_execution_score": "Magellan_Execution_Score__c",
      "magellan_vision_sentiment": "Magellan_Vision_Sentiment__c",
      "magellan_alignment_sentiment": "Magellan_Alignment_Sentiment__c",
      "magellan_execution_sentiment": "Magellan_Execution_Sentiment__c"
    }
    var obj = {}
    var objField = expansion_fields[field]
    if (objField == undefined) {
      console.log('Field not found!', field);
      return new Promise(function(resolve, reject){
        reject("Field not found");
      });
    }

    if (value == null) {
      value = 'NULL';
    }

    obj[objField] = value;

    return csRestangular
      .one('sfdc/expansion_rating', id)
      .put(obj)
      .then(results => {
      })
      .catch(err => {
        console.error(err);
      });
  }

  this.getExpansionRatingModels = function () {

    return Restangular
      .all('sfdc/expansion_rating_models')
      .getList()
      .then(models => {
        return models;
      })
      .catch(err => {
        throw err;
      });

  }

  this.getExpansionRatingModel = function (expansionRatingModelId) {

    return Restangular
      .one('sfdc/expansion_rating_models', expansionRatingModelId)
      .get()
      .then(model => {
        return model;
      })
      .catch(err => {
        throw err;
      });

  }

  this.expansionScoringFields = [
    "budget",
    "api_count",
    "etp_level",
    "gcp_growth",
    "api_traffic",
    "deal_timing",
    "api_dev_count",
    "license_value",
    "tech_capability",
    "sales_engagement",
    "new_license_value",
    "executive_commitment",
    "magellan_vision_score",
    "magellan_alignment_score",
    "magellan_execution_score",
    "magellan_vision_sentiment",
    "magellan_alignment_sentiment",
    "magellan_execution_sentiment"
  ];

  this.scoreExpansionRating = function (expansionRating, expansionRatingModel) {

    let score = 0;
    let original_score = expansionRating['priority_score'];
    let items = [];
    _.each(self.expansionScoringFields, function (key) {
      if (expansionRating.hasOwnProperty(key) && expansionRatingModel.hasOwnProperty(key)) {
        let elementRating = parseInt(expansionRating[key], 10);
        let elementModel = parseInt(expansionRatingModel[key], 10);
        items.push('' + elementModel + '*' + elementRating);
        if (!isNaN(elementModel) && !isNaN(elementRating)) score += elementModel * elementRating;

      }
    });
    // if( original_score > 0) {
    //   console.log(original_score, '->', score);
    //   console.log(items);
    //   console.log(expansionRating);
    //   console.log(expansionRatingModel);      
    // }
    return score;

  }

  this.getExpansionRating = function (expansionRatingId) {

    return Restangular
      .one('sfdc/expansion_ratings', expansionRatingId)
      .get()
      .then(rating => {
        return rating;
      })
      .catch(err => {
        console.error(err);
        throw err;
      });

  }

  this.getExpansionRatings = function () {

    return Restangular
      .all('cs/current_expansion_ratings')
      .getList()
      .then(ratings => {
        return ratings;
      });
  }

  this.getAccountExpansionRatings = function (account_id) {

    return Restangular
      .all('sfdc/expansion_ratings')
      .getList({
        where: ['account_id:eq:' + account_id],
        orderby: ['plan_month:desc']
      })
      .then(scores => {

        return scores;
      })
  }

  this.getCsRegions = function () {

    return Restangular
      .all('sfdc/cs_regions')
      .getList()
      .then(regions => {
        return regions;
      });

  }

  this.getBundleLint = function (bundlelint_id) {

    return Restangular
      .one('edge/org_api_revision_bundlelints', bundlelint_id)
      .get()
      .then(item => {
        return item;
      });

  }

  this.getLatestBundleLints = function (account_id) {

    return Restangular
      .all('edge/org_api_bundlelint_latest_revisions')
      .getList({
        where: ['account_id:eq:' + account_id],
        orderby: ['createddate:desc'],
        column: [
          'createddate',
          'id',
          'account_id',
          'org_name',
          'api_name',
          'revision',
          'bundlelint_id',
          'label',
          'score',
          'treatments',
          'normalized_score'
        ]
      })
      .then(list => {
        return list;
      });

  };

  this.getExplansionPlanningModels = function () {
    var endpoint = 'expansion_planning_models';

    if (ENV == 'local') {
      endpoint = 'cs/' + endpoint;
    }

    return csRestangular
      .all(endpoint)
      .getList(list => {
        return list;
      });

  };

  this.saveExpansionPlanningModel = function (name, model) {

    var expansion_planning_model = {
      shortdesc: name,
      entity: model
    };

    console.log(expansion_planning_model)

    return csRestangular
      .all('expansion_planning_models')
      .post(expansion_planning_model)
      .then(plan => {
        return true;
      })
      .catch(err => {
        console.error(err);
        throw Error('Error creating Account Plan!');
      });

  };

  this.getExpansionPlanningModel = function (id) {

    if (ENV == 'local') {
      var deferred = $q.defer();

      self.getExplansionPlanningModels()
        .then(presets => {
          var res = _.find(presets, p => p.id == id);
          deferred.resolve(res);
        });

      return deferred.promise;
    }

    return csRestangular
      .one('expansion_planning_models', id)
      .get(model => {
        return model;
      });

  };

  this.updateExpansionPlanningModel = function (id, name, model) {

    var expansion_planning_model = {
      shortdesc: name,
      entity: model
    };

    return csRestangular
      .one('expansion_planning_models', id)
      .customPUT(expansion_planning_model)
      .then(plan => {
        return true;
      })
      .catch(err => {
        console.error(err);
        throw Error('Error updating Account Plan!');
      });

  };

  this.deleteExpansionPlanningModel = function (id) {

    return csRestangular
      .one('expansion_planning_models', id)
      .remove()
      .then(plan => {
        return true;
      })
      .catch(err => {
        console.error(err);
        throw Error('Error removing Account Plan!');
      });

  };

  this.defaultPresetState = require('./defaultPresetState.js');

  this.expansionPlanningSaving = null;

  this.setExpansionPlanningSaving = function (name) {
    self.expansionPlanningSaving = name;
  }

  this.getExpansionPlanningSaving = function () {
    return self.expansionPlanningSaving;
  }

  this.clearExpansionPlanningSaving = function () {
    self.expansionPlanningSaving = null;
  }

};
