'use strict';

/**
 * @ngInject
 */
module.exports = function ($stateParams, Restangular, promiseTracker) {

  this.getCustomerSuccessContacts = function () {
  	var promise = Restangular
		  .one('accounts', $stateParams.accountId)
		  .all('cscontacts')
		  .getList({
		    orderby: 'name_first'
		  });
		return promise;
	}

  this.getAccountContacts = function () {
  	var promise = Restangular
	    .one('accounts', $stateParams.accountId)
	    .all('contacts')
	    .getList({
	      orderby: 'name_first'
	    });
	  return promise;
	}

  this.get360Users = function () {
  	var promise = Restangular
	    .one('accounts', $stateParams.accountId)
	    .all('360_users')
	    .getList({});

	  return promise;
	}

}
