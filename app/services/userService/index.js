'use strict';

module.exports = angular.module('app.customers.services.userService', [])
  .service('userService', require('./service'));