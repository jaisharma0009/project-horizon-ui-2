'use strict';

module.exports = class RelevantInformationController {
  constructor($scope, $state, HorizonService) {
    'ngInject';

    this.HorizonService = HorizonService;
    this.info = {};
    this.$scope = $scope;
  }

  $onInit() {
    //this.getRelevantInfo(this.account);

    this.$scope.$watch(() => this.account, (newVal, oldVal) => {
      if(newVal) {
        this.getRelevantInfo(newVal);
      }
    });
  }

  getRelevantInfo(account) {
    let rInfoTracker = this.HorizonService
        .getRelevantInfo(account)
        .then(info => {
          this.info = info.results;
        })

  }
}