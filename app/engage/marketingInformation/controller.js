'use strict';

module.exports = class MarketingInformationController {
  constructor($scope, $state, HorizonService) {
    'ngInject';

    this.HorizonService = HorizonService;
    this.info = {};
    this.$scope = $scope;
  }

  $onInit() {
    this.$scope.$watch(() => this.account, (newVal, oldVal) => {
      if(newVal) {
        this.getMarketingInfo(newVal);
      }
    });
  }

  getMarketingInfo(account) {
    let rInfoTracker = this.HorizonService
        .getMarketingInfo(account)
        .then(info => {
          this.info = info;
        })

  }
}