var template = require('./engage.template.html');
var EngageController = require('./engage.controller');

let EngageComponent = {
  restrict: 'E',
  template,
  controller: EngageController,
  controllerAs: 'engage'
};

module.exports = EngageComponent;