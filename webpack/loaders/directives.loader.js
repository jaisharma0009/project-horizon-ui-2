'use strict';

var path = require('path');
var utils = require('./utils');
var camelCase = require('lodash').camelCase;

module.exports = function (input) {
  this.cacheable && this.cacheable();

  var fileName = path.basename(this.resourcePath, '.directive.js');
  var directiveName = `${camelCase(fileName)}Directive`;
  var controllerName = utils.capitalize(directiveName);

  if (this.resourcePath.indexOf('.directive.js') < 0) {
    return input;
  }

  return input + `
    if (module.hot) {
     const name = ${controllerName}.name;
      module.hot.accept();
      // get controller instance
      
      // don't do anything if the directive is not printed
      const doc = angular.element(document.body);
      const injector = doc.injector();
      if (injector) {
        const $compile = injector.get('$compile');
        const directive = injector.get('${directiveName}')[0];
        if (directive) {
          // const origin = directive.constructor;
          const origin = directive;
          const target = ${controllerName}.prototype;
          const enumAndNonenum = Object.getOwnPropertyNames(target);
          const enumOnly = Object.keys(target);

          // not found in enumOnly keys mean the key is non-enumerable,
          // so return true so we keep this in the filter if it's not the constructor
          const nonenumOnly = enumAndNonenum.filter(key => enumOnly.indexOf(key) === -1 && key !== 'constructor');
          nonenumOnly.forEach(val => origin.__proto__[val] = target[val]);

          // trigger rootscope update
          doc.scope().$apply();
          console.info('Hot Swapped directive ' + name);
        }
      }
    }
  `;
  
};