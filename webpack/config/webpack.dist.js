import webpack         from 'webpack';
import merge           from 'webpack-merge';
import UglifyJsPlugin  from 'uglifyjs-webpack-plugin';
import webpackCommon   from './webpack.common';
import config          from './config';

export default function (options) {
  let commonConfig = webpackCommon(options);

  return merge(commonConfig, {
    devtool: 'source-map',

    plugins: [
      new UglifyJsPlugin({
        sourceMap: true,
        parallel: true,
        include: [ config.paths.app ]
      })
    ]
  });
};
