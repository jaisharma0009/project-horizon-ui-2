'use strict';

var config = require('./config');

module.exports = {
  // used by gulpfile
  // hostname: 'localhost',
  port: 9089,
  host: 'localhost',
  // port: 443,
  // https: true,
  inline: true,
  hot: true,

  contentBase: config.paths.static,

  stats: {
    colors: true
  }
};
