'use strict';

var moment = require('moment-fquarter');

module.exports = function(Highcharts) {
  Highcharts.fiscalYearStartMonth = 8;

  Highcharts.dateFormats = Highcharts.dateFormats || {};

  // Week number
  Highcharts.dateFormats.W = function(timestamp) {
    return moment(timestamp).week();
  };

  // ISO week number
  Highcharts.dateFormats.V = function(timestamp) {
    return moment(timestamp).isoWeek();
  };

  // Quarter number
  Highcharts.dateFormats.Q = function(timestamp) {
    return moment(timestamp).quarter();
  };

  //
  // Fiscal
  //

  // Fiscal week number
  Highcharts.dateFormats.FW = function(timestamp) {
    return moment(timestamp)
      .fquarter(Highcharts.fiscalYearStartMonth)
      .week;
  };

  // Fiscal quarter number
  Highcharts.dateFormats.FQ = function(timestamp) {
    return moment(timestamp)
      .fquarter(Highcharts.fiscalYearStartMonth)
      .quarter;
  };

  // Fiscal year (calendar start)
  Highcharts.dateFormats.FY = function(timestamp) {
    return moment(timestamp)
      .fquarter(Highcharts.fiscalYearStartMonth)
      .year;
  };

  // Fiscal year (calendar start/end)
  Highcharts.dateFormats.FFY = function(timestamp) {
    return moment(timestamp)
      .fquarter(Highcharts.fiscalYearStartMonth)
      .fiscalYear;
  };

  // Fiscal week, quarter, calendar year start/end
  Highcharts.dateFormats.FL = function(timestamp) {
    var fquarter = moment(timestamp)
      .fquarter(Highcharts.fiscalYearStartMonth);

    return 'W' + fquarter.week +
      ' Q' + fquarter.quarter +
      ' ' + fquarter.fiscalYear;
  };
};
