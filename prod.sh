#
# Generates updated ui code and deploys it to production server
#
#   Assumes that you have a config entry in ~/.ssh/config for 360 that points to the
#     production server
#

#remove last zip file
rm dist.zip

#create new one
zip -r dist.zip dist

#scp it to prod
scp dist.zip 360:

ssh 360 "unzip -o dist.zip"
