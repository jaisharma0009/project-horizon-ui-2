var autoprefixer = require('autoprefixer');

module.exports = function (ctx) {
  return {
    plugins: [
      autoprefixer({
        browsers: ['last 2 versions']
      })
    ]
  };
};
