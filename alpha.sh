#
# Generates updated ui code and deploys it to the alpha server
#
#   Assumes that you have a config entry in ~/.ssh/config for alpha360 that points to the
#     alpha server
#

#generate new source
gulp dist

#remove last zip file
rm dist.zip

#create new one
zip -r dist.zip dist

#scp it to prod
scp dist.zip alpha360:

ssh alpha360 "unzip -o dist.zip"
